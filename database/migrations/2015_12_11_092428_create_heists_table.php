<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('difficulty');
            $table->integer('money_max');
            $table->integer('heat');
            $table->timestamps();
        });

        Schema::create('heist_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('heist_id')->unsigned();
            $table->string('name');
            $table->string('attribute');
            $table->timestamps();

            $table->foreign('heist_id')->references('id')->on('heists')->onDelete('cascade');
        });

        //Burglary
        $id = DB::table('heists')->insertGetId(['difficulty' => 10, 'money_max' => 400, 'heat' => 20, 'name' => 'Burglary']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'dex', 'name' => 'Locksmith']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'int', 'name' => 'Planner']);

        //Stickup
        $id = DB::table('heists')->insertGetId(['difficulty' => 10, 'money_max' => 600, 'heat' => 30, 'name' => 'Stickup']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'cha', 'name' => 'Fast talker']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'str', 'name' => 'Muscle']);

        //Ram raid
        $id = DB::table('heists')->insertGetId(['difficulty' => 20, 'money_max' => 1500, 'heat' => 40, 'name' => 'Ram raid']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'dex', 'name' => 'Driver']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'str', 'name' => 'Bag-man']);

        //Short con
        $id = DB::table('heists')->insertGetId(['difficulty' => 20, 'money_max' => 1000, 'heat' => 30, 'name' => 'Short con']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'cha', 'name' => 'Grifter']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'int', 'name' => 'Hacker']);

        //Bank robbery
        $id = DB::table('heists')->insertGetId(['difficulty' => 50, 'money_max' => 10000, 'heat' => 30, 'name' => 'Bank robbery']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'cha', 'name' => 'Front man']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'int', 'name' => 'Safe cracker']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'str', 'name' => 'Enforcer']);
        DB::table('heist_roles')->insert(['heist_id' => $id,  'attribute' => 'dex', 'name' => 'Driver']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('heist_roles');
        Schema::drop('heists');
    }
}
