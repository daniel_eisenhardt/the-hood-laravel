<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('x');
            $table->integer('y');
            $table->timestamps();
        });

        DB::table('cities')->insert(['name' => 'Amsterdam', 'x' => 323, 'y' => 66]);
        DB::table('cities')->insert(['name' => 'Paris', 'x' => 316, 'y' => 80]);
        DB::table('cities')->insert(['name' => 'London', 'x' => 311, 'y' => 68]);
        DB::table('cities')->insert(['name' => 'Sydney', 'x' => 645, 'y' => 282]);
        DB::table('cities')->insert(['name' => 'Bogotá', 'x' => 136, 'y' => 187]);
        DB::table('cities')->insert(['name' => 'Hong kong', 'x' => 572, 'y' => 140]);
        DB::table('cities')->insert(['name' => 'Kabul', 'x' => 462, 'y' => 110]);
        DB::table('cities')->insert(['name' => 'Moscow', 'x' => 390, 'y' => 58]);
        DB::table('cities')->insert(['name' => 'Cape Town', 'x' => 354, 'y' => 280]);
        DB::table('cities')->insert(['name' => 'Dubai', 'x' => 432, 'y' => 137]);
        DB::table('cities')->insert(['name' => 'Cairo', 'x' => 379, 'y' => 127]);
        DB::table('cities')->insert(['name' => 'New York', 'x' => 150, 'y' => 95]);
        DB::table('cities')->insert(['name' => 'Chicago', 'x' => 125, 'y' => 95]);
        DB::table('cities')->insert(['name' => 'Miami', 'x' => 127, 'y' => 132]);
        DB::table('cities')->insert(['name' => 'Las Vegas', 'x' => 53, 'y' => 107]);
        DB::table('cities')->insert(['name' => 'Seattle', 'x' => 62, 'y' => 82]);
        DB::table('cities')->insert(['name' => 'Buenos Aires', 'x' => 180, 'y' => 291]);
        DB::table('cities')->insert(['name' => 'São Paulo', 'x' => 202, 'y' => 253]);
        DB::table('cities')->insert(['name' => 'Tokyo', 'x' => 618, 'y' => 108]);
        DB::table('cities')->insert(['name' => 'Abuja', 'x' => 327, 'y' => 173]);
        DB::table('cities')->insert(['name' => 'Marrakesh', 'x' => 294, 'y' => 118]);
        DB::table('cities')->insert(['name' => 'Mogadishu', 'x' => 413, 'y' => 193]);
        DB::table('cities')->insert(['name' => 'New Delhi', 'x' => 484, 'y' => 132]);
        DB::table('cities')->insert(['name' => 'Beijing', 'x' => 563, 'y' => 94]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cities');
    }
}
