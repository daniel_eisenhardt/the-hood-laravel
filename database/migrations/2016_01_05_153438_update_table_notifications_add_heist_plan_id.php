<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableNotificationsAddHeistPlanId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications', function ($table) {
            $table->integer('heist_plan_id')->nullable()->unsigned();

            $table->foreign('heist_plan_id')->references('id')->on('heist_plans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications', function ($table) {
            $table->dropForeign('notifications_heist_plan_id_foreign');

            $table->dropColumn('heist_plan_id');
        });
    }
}
