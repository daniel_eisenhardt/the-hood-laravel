<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableAddAttibuteXp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->integer('cha_xp');
            $table->integer('dex_xp');
            $table->integer('int_xp');
            $table->integer('str_xp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('cha_xp');
            $table->dropColumn('dex_xp');
            $table->dropColumn('int_xp');
            $table->dropColumn('str_xp');
        });
    }
}
