<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHustlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hustles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('attribute');
            $table->integer('difficulty');
            $table->integer('money_max');
            $table->integer('heat');
            $table->timestamps();
        });

        DB::table('hustles')->insert(['difficulty' => 3,  'money_max' => 20, 'heat' => 2, 'attribute' => 'cha', 'name' => 'thai gem con']);
        DB::table('hustles')->insert(['difficulty' => 6,  'money_max' => 20, 'heat' => 1, 'attribute' => 'dex', 'name' => 'card trick bet']);
        DB::table('hustles')->insert(['difficulty' => 5,  'money_max' => 20, 'heat' => 2, 'attribute' => 'int', 'name' => 'change raising']);
        DB::table('hustles')->insert(['difficulty' => 4,  'money_max' => 30, 'heat' => 5, 'attribute' => 'str', 'name' => "extort a 'gift'"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hustles');
    }
}
