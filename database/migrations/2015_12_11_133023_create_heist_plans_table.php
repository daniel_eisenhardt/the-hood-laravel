<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeistPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heist_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('heist_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('status');
            $table->integer('money')->nullable();
            $table->timestamps();

            $table->foreign('heist_id')->references('id')->on('heists')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });


        Schema::create('heist_participations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('heist_plan_id')->unsigned();
            $table->integer('heist_role_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('status');
            $table->timestamps();

            $table->foreign('heist_plan_id')->references('id')->on('heist_plans')->onDelete('cascade');
            $table->foreign('heist_role_id')->references('id')->on('heist_roles')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('heist_participations');
        Schema::drop('heist_plans');
    }
}
