<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('difficulty');
            $table->integer('money_max');
            $table->integer('heat');
            $table->timestamps();
        });

        DB::table('jobs')->insert(['difficulty' => 1,  'money_max' => 10, 'heat' => 1, 'name' => 'shoplift']);
        DB::table('jobs')->insert(['difficulty' => 3,  'money_max' => 25, 'heat' => 2, 'name' => 'pickpocket']);
        DB::table('jobs')->insert(['difficulty' => 5,  'money_max' => 40, 'heat' => 3, 'name' => 'alleyway mugging']);
        DB::table('jobs')->insert(['difficulty' => 10, 'money_max' => 80, 'heat' => 5, 'name' => 'gas station stickup']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}
