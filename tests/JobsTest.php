<?php

use \App\Models\Job;

class JobsTest extends ApiTester
{

    public function setUp()
    {
        parent::setUp();

        $user = new \App\Models\User();
        $user->energy = \Illuminate\Support\Facades\Config::get('rules.energy_max');
        $user->city_id = \App\Models\City::firstOrFail()->id;
        $user->save();

        $this->setUser($user);
    }

    /**
     * @test
     */
    public function it_fetches_jobs()
    {
        $data = $this->getJson('api/1.0/jobs');

        $this->assertResponseOk();
        $this->assertCount(4, $data->response_data, 'api/1.0/jobs should return 4 jobs');
    }

    /**
     * @test
     */
    public function it_executes_job_success()
    {
        $job = new Job();
        $job->difficulty = 0;
        $job->money_max = 10;
        $job->save();

        $user = \App\Models\User::where('id', $this->activeUser->id)->first();
        $energy = $user->energy;
        $money = $user->money;
        $xp = $user->xp;

        $data = $this->postJson('api/1.0/jobs/' . $job->id . '/execute');
        $user = \App\Models\User::where('id', $this->activeUser->id)->first();

        $this->assertResponseOk();
        $this->assertTrue($data->response_data->result->success, 'A job with 0 difficulty should always succeed');
        $this->assertEquals( (int) $energy-1, (int) $user->energy, 'Executing a job should cost the user 1 energy' );
        $this->assertEquals( (int) $xp+1, (int) $user->xp, 'It should add one xp to the user' );
        $this->assertEquals( (int) $money + $data->response_data->result->reward, $user->money, 'It should add the job reward to the user\'s money');
    }

    /**
     * @test
     */
    public function it_executes_job_failure()
    {
        $job = new Job();
        $job->difficulty = 2147483647;
        $job->heat = 4;
        $job->save();

        $user = \App\Models\User::where('id', $this->activeUser->id)->first();
        $heat = $user->reputation->heat;

        $this->postJson('api/1.0/jobs/' . $job->id . '/execute');
        $user = \App\Models\User::where('id', $this->activeUser->id)->with(['reputation'])->first();

        $this->assertResponseOk();
        $this->assertEquals( (int) $heat + (int) $job->heat, (int) $user->reputation->heat, 'Failing a job should increment the user\'s heat by the job\'s heat' );
    }

    /**
     * @test
     */
    public function it_fails_execution_when_out_of_energy()
    {
        $this->activeUser->energy = 0;
        $this->activeUser->save();

        $data = $this->postJson('api/1.0/jobs/' . Job::firstOrFail()->id . '/execute');

        $this->assertResponseStatus(400);
    }
}