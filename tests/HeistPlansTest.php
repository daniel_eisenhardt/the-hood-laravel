<?php


class HeistPlansTest extends ApiTester
{

    protected $user1;

    protected $user2;

    public function setUp()
    {
        parent::setUp();

        $user1 = new \App\Models\User();
        $user1->city_id = \App\Models\City::firstOrFail()->id;
        $user1->save();
        $this->user1 = $user1;

        $user2 = new \App\Models\User();
        $user2->city_id = \App\Models\City::firstOrFail()->id;
        $user2->save();
        $this->user2 = $user2;

        $this->setUser($user1);
    }

    /**
     * @test
     */
    public function it_creates_heist_plans()
    {
        $heist = \App\Models\Heist::with('heistRoles')->firstOrFail();

        $input = [
            'heist_id' => $heist->id,
            'roles' => [
                $heist->heistRoles[0]->id => $this->user1->id,
                $heist->heistRoles[1]->id => $this->user2->id,
            ]
        ];
        $output = $this->postJson('api/1.0/heist_plans', $input);

        $this->assertResponseOk();
        $this->assertNotNull($output->response_data->user->heist_plan);
    }
}