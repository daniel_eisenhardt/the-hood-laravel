<?php


class HeistsTest extends ApiTester
{

    public function setUp()
    {
        parent::setUp();

        $user = new \App\Models\User();
        $user->city_id = \App\Models\City::firstOrFail()->id;
        $user->save();

        $this->setUser($user);
    }

    /**
     * @test
     */
    public function it_fetches_heists()
    {
        $data = $this->getJson('api/1.0/heists');

        $this->assertResponseOk();
        $this->assertCount(5, $data->response_data, 'api/1.0/heists should return 5 heists');
    }
}