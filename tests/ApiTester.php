<?php

use Faker\Factory as Faker;
use Illuminate\Support\Facades\Artisan;

class ApiTester extends TestCase
{

    protected $fake;

    protected $times = 1;

    protected $headers = ['Accept' => 'application/json'];

    protected $activeUser;

    private static $seeded = false;

    /**
     * ApiTester constructor.
     */
    public function __construct()
    {
        $this->fake = Faker::create();
    }

    public function setUp()
    {
        parent::setUp();

        if(!self::$seeded) {
            Artisan::call('migrate:reset');
            Artisan::call('migrate');

            self::$seeded = true;
        }
    }

    /**
     * @param $count
     * @return $this
     */
    protected function times($count)
    {
        $this->times = $count;

        return $this;
    }

    /**
     * @param $uri
     * @return string
     */
    protected function getJson($uri)
    {
        return json_decode($this->call('GET', $uri)->getContent());
    }

    protected function postJson($uri, $data=[])
    {
        return json_decode($this->call('POST', $uri, $data)->getContent());
    }

    protected function setUser(\App\Models\User $user)
    {
        $token = JWTAuth::fromUser($user);
        \Tymon\JWTAuth\Facades\JWTAuth::setToken($token);
        $this->headers['Authorization'] = 'Bearer '.$token;

        $this->activeUser = $user;
    }
}