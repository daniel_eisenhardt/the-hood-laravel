<?php


namespace App\Services;

use App\Models\Notification;
use App\Models\User;

/**
 * Class NotificationService
 * @package App\Services
 */
class NotificationService
{

    /**
     * @param $type
     * @param array $data
     * @param User $reciever
     * @param User|null $sender
     * @return Notification
     */
    public function create(User $reciever, User $sender=null, $type, array $data, $facebookRequestOid=null)
    {
        $notification = new Notification();
        $notification->user_id = $reciever->id;
        $notification->sender_id = $sender == null ? null : $sender->id;
        $notification->type = $type;
        $notification->data = $data;
        $notification->facebook_request_oid = $facebookRequestOid;

        return $notification;
    }
}