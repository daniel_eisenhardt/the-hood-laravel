<?php


namespace App\Services;

use App\Models\City;
use \App\Models\User;
use Illuminate\Support\Facades\Config;

class UserService
{
    /**
     * @var Calculator
     */
    protected $calculator;

    /**
     * UserService constructor.
     * @param Calculator $calculator
     */
    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    /**
     * @param \Facebook\GraphNodes\GraphUser $graphUser
     * @return \App\Models\User
     */
    public function getUserFromGraphUser(\Facebook\GraphNodes\GraphUser $graphUser)
    {
        $user = User::where('facebook_id', $graphUser->getId())->first();

        if($user === null)
        {
            $user = new User();

            $user->facebook_id = $graphUser->getId();
            $user->first_name = $graphUser->getFirstName();
            $user->last_name = $graphUser->getLastName();
            $user->energy = Config::get('rules.energy_max');
            $user->city_id = City::first()->id;

            $user->save();
        }

        return $user;
    }

    public function storeFacebookAccessToken(\App\Models\User $user, \Facebook\Authentication\AccessToken $accessToken)
    {
        $user->facebook_token_value = $accessToken->getValue();
        $user->facebook_token_expire_time = $accessToken->getExpiresAt()->getTimestamp();
        $user->save();
    }

    public function getLevel(User $user)
    {
        return $this->calculator->getLevel($user->xp);
    }

    public function getAttributeLevel(User $user, $attribute)
    {
        switch($attribute)
        {
            case 'cha':
                $xp  = $user->cha_xp;
                break;
            case 'dex':
                $xp  = $user->dex_xp;
                break;
            case 'int':
                $xp  = $user->int_xp;
                break;
            case 'str':
                $xp  = $user->str_xp;
                break;
            default:
                throw new \Exception('Unknown attribute ' . $attribute);
        }

        return $this->calculator->getLevel($xp);
    }

    public function addAttributeXp(User $user, $attribute, $xp)
    {
        switch($attribute)
        {
            case 'cha':
                $user->cha_xp += $xp;
                break;
            case 'dex':
                $user->dex_xp += $xp;
                break;
            case 'int':
                $user->int_xp += $xp;
                break;
            case 'str':
                $user->str_xp += $xp;
                break;
            default:
                throw new \Exception('Unknown attribute ' . $attribute);
        }
    }
}