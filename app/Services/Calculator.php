<?php


namespace App\Services;


/**
 * Class Calculator
 * @package App\Services
 */
class Calculator
{

    /**
     * @param $power
     * @param $difficulty
     * @return bool
     */
    public function randSuccess($power, $difficulty)
    {
        $granularity = pow(10,6);
        return mt_rand(0, $granularity) < $this->successChance($power, $difficulty) * $granularity;
    }

    /**
     * @param $power
     * @param $difficulty
     * @return float
     */
    private function successChance($power, $difficulty)
    {
        $power = pow($power, 2);
        $difficulty = pow($difficulty, 2);

        return $power / ($power+$difficulty);
    }

    /**
     * @param $xp
     * @return int
     */
    public function getLevel($xp)
    {
        $level = 1;
        while($this->xpForLevel($level) <= $xp) {
            $level ++;
        }
        return $level-1;
    }

    /**
     * @param $level
     * @return float
     */
    private function xpForLevel($level)
    {
        $base = 15;

        $base *= 75/83; //make the base the level 1 xp
        $outcome = floor(($base * pow(2, $level/7)) / (pow(2, 1/7) -1) + (1/8)*pow($level,2) - $level/8 - ($base*2)/(2-pow(2, (6/7))));

        return floor($outcome);
    }

    /**
     * Calculates the distance between two objects that both have an x and y attribute
     *
     * @param $object1
     * @param $object2
     * @return float
     */
    public function getDistance($object1, $object2)
    {
        return sqrt(
            pow(abs( $object1->x - $object2->x ), 2) +
            pow(abs( $object1->y - $object2->y ), 2)
        );
    }
}