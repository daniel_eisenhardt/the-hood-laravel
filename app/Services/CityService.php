<?php


namespace App\Services;

use App\Models\City;
use Illuminate\Support\Facades\Config;

class CityService
{
    /**
     * @var Calculator
     */
    protected $calculator;

    /**
     * CityService constructor.
     * @param Calculator $calculator
     */
    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    /**
     * @param City $origin
     * @param City $destination
     * @return int
     */
    public function getTicketPrice(City $origin, City $destination)
    {
        return ceil($this->calculator->getDistance($origin, $destination)) * Config::get('rules.travel_price');
    }
}