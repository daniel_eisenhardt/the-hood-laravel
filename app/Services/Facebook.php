<?php


namespace App\Services;


use App\Models\User;
use \Facebook\Authentication\AccessToken;
use Mockery\CountValidator\Exception;

/**
 * Class Facebook
 * @package App\Services
 */
class Facebook
{

    /**
     * @var \SammyK\LaravelFacebookSdk\LaravelFacebookSdk sdk
     */
    private $sdk;

    /**
     * @var \Facebook\Authentication\AccessToken;
     */
    private $accessToken;

    /**
     * Facebook constructor.
     * @param \SammyK\LaravelFacebookSdk\LaravelFacebookSdk $facebookSdk
     */
    public function __construct(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $facebookSdk)
    {
        $this->sdk = $facebookSdk;
    }

    /**
     * @param $tokenValue
     * @param int $expiresAt
     */
    public function setToken($tokenValue, $expiresAt = 0)
    {
        $token = new AccessToken($tokenValue, $expiresAt);

        if($token->isLongLived()) {
            $this->accessToken = $token;
        }
        else {
            $this->accessToken = $this->sdk->getOAuth2Client()->getLongLivedAccessToken($token);
        }

        $this->sdk->setDefaultAccessToken( $this->accessToken );

        return $this;
    }

    /**
     * @param User $user
     * @return Facebook
     */
    public function setTokenFromUser(User $user )
    {
        return $this->setToken($user->facebook_token_value, $user->facebook_token_expires_at);
    }

    /**
     * @return \Facebook\Authentication\AccessToken
     */
    public function getToken()
    {
        return $this->accessToken;
    }

    /**
     * @return \Facebook\GraphNodes\GraphUser
     */
    public function getGraphUser()
    {
        $response = $this->sdk->get('/me?fields=id,first_name,last_name');

        $data = $response->getGraphUser();

        return $data;
    }

    public function getFriendIds()
    {
        $response = $this->sdk->get('/me/friends')->getDecodedBody()['data'];

        $response = array_map(function($record) { return $record['id']; }, $response);

        return $response;
    }

    public function requestsExist($requestOid, $users)
    {
        foreach($users as $user) {
            try {
                $response = $this->sdk->get('/' . $requestOid . '_' . $user->facebook_id);
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                return false;
            }
        }

        return true;
    }

    public function deleteRequest($requestOid, $recipient)
    {
        try {
            //use an app access token for this action, because users can only delete apprequests send to them
            $this->sdk->setDefaultAccessToken( $this->sdk->getApp()->getAccessToken() );

            $this->sdk->delete('/' . $requestOid . '_' . $recipient->facebook_id);

            $this->sdk->setDefaultAccessToken( $this->accessToken );
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            return false;
        }

        return true;
    }
}