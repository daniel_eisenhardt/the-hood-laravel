<?php


namespace App\Services;


use App\Models\HeistPlan;

/**
 * Class HeistService
 * @package App\Services
 */
class HeistService
{
    /**
     * @var Calculator
     */
    protected $calculator;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * HeistService constructor.
     * @param Calculator $calculator
     * @param UserService $userService
     */
    public function __construct(Calculator $calculator, UserService $userService)
    {
        $this->calculator = $calculator;
        $this->userService = $userService;
    }

    public function execute(HeistPlan $heistPlan)
    {
        $heist = $heistPlan->heist;
        $leader = $heistPlan->user;
        $success = $this->randSuccess($heistPlan);
        $result = ['success' => $success];

        if($success) {
            $heistPlan->status = 'succeeded';
            $reward = mt_rand( ceil($heist->money_max/2), $heist->money_max );
            $leader->money += $reward;
            $leader->save();
            $result['reward'] = $reward;
        } else {
            $heistPlan->status = 'failed';
            foreach ($heistPlan->heistParticipations as $heistParticipation)
            {
                $heistParticipation->user->addHeatAndSave( $heist->heat );
            }
        }
        $heistPlan->save();

        return $result;
    }

    /**
     * @param HeistPlan $heistPlan
     * @return bool
     */
    public function isReady(HeistPlan $heistPlan)
    {
        $ready = true;
        foreach ($heistPlan->heistParticipations as $heistParticipation)
        {
            if($heistParticipation->status !== 'accepted') {
                $ready = false;
                break;
            }
        }

        return $ready;
    }

    private function randSuccess(HeistPlan $heistPlan)
    {
        $powers = [];
        foreach ($heistPlan->heistParticipations as $heistParticipation)
        {
            $powers[] = $this->userService->getLevel($heistParticipation->user);
            $powers[] = $this->userService->getAttributeLevel($heistParticipation->user, $heistParticipation->heistRole->attribute);
        }
        $power = array_sum($powers) / count($powers);

        return $this->calculator->randSuccess($power, $heistPlan->heist->difficulty);
    }
}