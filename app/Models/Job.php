<?php

namespace App\Models;


class Job extends BaseModel
{
    public $fillable = ['name', 'difficulty', 'money_max', 'heat'];
}