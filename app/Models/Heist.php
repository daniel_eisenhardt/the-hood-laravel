<?php


namespace App\Models;


class Heist extends BaseModel
{

    public function heistRoles()
    {
        return $this->hasMany('App\Models\HeistRole');
    }
}