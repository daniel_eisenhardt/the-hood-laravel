<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as BaseUser;
use Illuminate\Support\Facades\Config;

/**
 * Class User
 * @package App\Models
 */
class User extends BaseUser
{

    /**
     * @var \App\Models\Reputation
     */
    protected $reputation;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reputations()
    {
        return $this->hasMany('\App\Models\Reputation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reputation()
    {
        return $this->hasOne('\App\Models\Reputation')->where('city_id', $this->city_id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany('\App\Models\Notification');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function heistPlan()
    {
        return $this->hasOne('App\Models\HeistPlan')->with('heistParticipations')->where('status', 'planned');
    }

    /**
     * @return mixed
     */
    public function heistPlans()
    {
        return $this->belongsToMany('\App\Models\HeistPlan', 'heist_participations')
            ->with('heistParticipations')
            ->where('heist_plans.status', 'planned')
            ->where('heist_plans.user_id', '!=', $this->id);
    }

    public function heistParticipations()
    {
        return $this->hasMany('\App\Models\HeistParticipation');
    }

    /**
     * @return float|mixed
     */
    public function getEnergyAttribute()
    {
        $energy = $this->original['energy'];

        if($this->original['energy_time'] !== null) {
            $energy += floor((time() - strtotime($this->original['energy_time'])) / Config::get('rules.energy_time'));
        }

        $energy = min($energy, Config::get('rules.energy_max'));

        return $energy;
    }

    public function getEnergyTimeAttribute()
    {
        if($this->getEnergyAttribute() === Config::get('rules.energy_max')) {
            return null;
        }

        $time = strtotime($this->original['energy_time']);

        $time += floor((time()-strtotime($this->original['energy_time'])) / Config::get('rules.energy_time')) * Config::get('rules.energy_time');

        return date('Y-m-d H:i:s', $time);
    }

    public function getTravelTimeAttribute($travelTime)
    {
        if(strtotime($travelTime) < time()) {
            return null;
        }
        return $travelTime;
    }

    /**
     * @param $reputation
     * @return \App\Models\Reputation
     */
    public function getReputationAttribute()
    {
        if(empty($this->reputation) || $this->reputation->city_id !== $this->city_id) {
            $reputation = $this->reputation()->first();
            if(empty($reputation)) {
                $reputation = new Reputation();

                $reputation->user_id = $this->id;
                $reputation->city_id = $this->city_id;
                $reputation->heat = 0;
            }

            $this->reputation = $reputation;
        }

        return $this->reputation;
    }

    public function getArrestedAtAttribute($arrestedAt)
    {
        return strtotime($arrestedAt) + Config::get('rules.arrest_time') > time() ? $arrestedAt : null;
    }

    public function addHeatAndSave($heat)
    {
        $reputation = $this->getReputationAttribute();
        $oldHeat = $reputation->heat;

        $reputation->heat += $heat;

        if( floor($oldHeat/100) < floor($reputation->heat/100) ) {
            $this->arrested_at = date('Y-m-d H:i:s');
        }

        $reputation->save();
        $this->save();
    }
}