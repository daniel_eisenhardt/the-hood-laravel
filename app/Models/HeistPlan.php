<?php


namespace App\Models;


class HeistPlan extends BaseModel
{

    public function heist()
    {
        return $this->belongsTo('App\Models\Heist');
    }

    public function heistParticipations()
    {
        return $this->hasMany('App\Models\HeistParticipation');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function invitations()
    {
        return $this->hasMany('App\Models\Notification')->where('type', 'heistPlan-invitation');
    }
}