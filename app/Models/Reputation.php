<?php


namespace App\Models;

use Illuminate\Support\Facades\Config;

class Reputation extends BaseModel
{

    public function getHeatAttribute($heat)
    {
        if($this->left_at !== null) {
            $heat = $heat - floor((time() - strtotime($this->left_at)) / Config::get('rules.heat_time'));
        }

        return max($heat, 0);
    }
}