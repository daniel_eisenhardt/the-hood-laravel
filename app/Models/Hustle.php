<?php


namespace App\Models;


class Hustle extends BaseModel
{
    public $fillable = ['name', 'difficulty', 'money_max', 'heat'];
}