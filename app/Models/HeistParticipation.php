<?php


namespace App\Models;


class HeistParticipation extends BaseModel
{

    public function heistPlan()
    {
        return $this->belongsTo('App\Models\HeistPlan');
    }

    public function heistRole()
    {
        return $this->belongsTo('App\Models\HeistRole');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}