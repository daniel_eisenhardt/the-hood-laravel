<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/app');
});
Route::post('/', function () {
    return redirect('/app');
});

Route::group(['prefix' => 'api/1.0'], function()
{
    Route::get('auth/config', 'AuthController@config');
    Route::post('auth/login', 'AuthController@login');

    Route::group(['middleware' => ['jwt.auth']], function()
    {
        Route::get('auth/user', 'AuthController@user');

        Route::resource('jobs', 'JobsController', ['only' => 'index']);
        Route::resource('hustles', 'HustlesController', ['only' => 'index']);
        Route::resource('heists', 'HeistsController', ['only' => ['index', 'show']]);
        Route::resource('heist_plans', 'HeistPlansController', ['only' => ['show', 'store']]);
        Route::resource('cities', 'CitiesController', ['only' => ['index', 'show']]);
        Route::resource('users', 'UsersController', ['only' => ['show']]);

        Route::post('/bailout', 'BailController@execute');
        Route::post('notifications/{id}/markread', 'NotificationsController@markRead');

        Route::group(['middleware' => 'not_arrested'], function() {
            Route::post('jobs/{id}/execute', 'JobsController@execute');

            Route::post('hustles/{id}/execute', 'HustlesController@execute');

            Route::post('cities/{id}/travel', 'CitiesController@travel');

            Route::get('friends', 'UsersController@friends');

            Route::post('users/{id}/sendmoney', 'UsersController@sendmoney');
            Route::post('users/{id}/sendmessage', 'UsersController@sendmessage');

            Route::post('heist_plans/{id}/cancel', 'HeistPlansController@cancel');
            Route::post('heist_participations/{id}/accept', 'HeistParticipationsController@accept');
            Route::post('heist_participations/{id}/reject', 'HeistParticipationsController@reject');
            Route::post('heist_participations/{id}/cancel', 'HeistParticipationsController@cancel');
        });
    });
});