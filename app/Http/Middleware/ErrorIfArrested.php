<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class ErrorIfArrested
{

    public function handle($request, Closure $next)
    {
        if( strtotime(\Auth::user()->arrested_at) + Config::get('rules.arrest_time') > time() ) {
            return response()->json(['error' => ['message' => 'Still in jail!', 'status_code' => 400]], 400);
        }

        return $next($request);
    }
}