<?php


namespace App\Http\Controllers;


use App\Models\Heist;
use App\Transformers\HeistTransformer;

class HeistsController extends ApiController
{

    /**
     * @var HeistTransformer
     */
    protected $heistTransformer;

    /**
     * HeistsController constructor.
     * @param HeistTransformer $heistTransformer
     */
    public function __construct(HeistTransformer $heistTransformer)
    {
        parent::__construct();

        $this->heistTransformer = $heistTransformer;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $heist = Heist::where('id', $id)->first();

        return $this->respond($this->heistTransformer->transform($heist));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $heists = Heist::with('heistRoles')->get();

        return $this->respond( $this->heistTransformer->transformCollection($heists) );
    }
}