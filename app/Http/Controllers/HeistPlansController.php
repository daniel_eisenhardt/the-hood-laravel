<?php


namespace App\Http\Controllers;


use App\Models\Heist;
use App\Models\HeistParticipation;
use App\Models\HeistPlan;
use App\Models\User;
use App\Services\Facebook;
use App\Services\NotificationService;
use App\Transformers\HeistPlanTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

/**
 * Class HeistPlansController
 * @package App\Http\Controllers
 */
class HeistPlansController extends ApiController
{

    /**
     * @var HeistPlanTransformer
     */
    protected $heistPlanTransformer;

    /**
     * @var UserTransformer
     */
    protected $userTransformer;

    /**
     * @var NotificationService
     */
    protected $notificationService;

    /**
     * @var Facebook
     */
    protected $facebook;

    /**
     * HeistPlansController constructor.
     * @param HeistPlanTransformer $heistPlanTransformer
     * @param UserTransformer $userTransformer
     * @param NotificationService $notificationService
     * @param Facebook $facebook
     */
    public function __construct(HeistPlanTransformer $heistPlanTransformer, UserTransformer $userTransformer, NotificationService $notificationService, Facebook $facebook)
    {
        parent::__construct();

        $this->heistPlanTransformer = $heistPlanTransformer;
        $this->userTransformer = $userTransformer;
        $this->notificationService = $notificationService;
        $this->facebook = $facebook;
        $this->facebook->setTokenFromUser(Auth::user());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $heistPlan = HeistPlan::where('id', $id)->first();

        return $this->respond($this->heistPlanTransformer->transform($heistPlan));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        $user = Auth::user();
        $heist = Heist::where('id', Request::input('heist_id'))->with('heistRoles')->first();
        $roles = Request::input('roles');
        $requestOid = Request::input('request_oid');

        //validation
        if(!$heist) {
            return $this->respondInvalidRequest('Heist does not exist');
        }
        if($user->heistPlan !== null) {
            return $this->respondInvalidRequest('Already planning a Heist');
        }
        foreach($heist->heistRoles as $role) {
            if(empty($roles[ $role->id ])) {
                return $this->respondInvalidRequest('Missing role');
            }
        }
        if(count($roles) !== count(array_unique($roles))) {
            return $this->respondInvalidRequest('A user can only fill one role per Heist');
        }
        if(!in_array( $user->id, $roles )) {
            return $this->respondInvalidRequest('The inviting user must participate in their own Heist');
        }
        $invitees = User::whereIn('id', array_filter($roles, function($id) use ($user) {return $id != $user->id;}))->get();
        if( !$this->facebook->requestsExist($requestOid, $invitees) ) {
            return $this->respondInvalidRequest('Facebook requests do not exist');
        }

        //create HeistPlan
        $heistPlan = new HeistPlan();
        $heistPlan->heist_id = $heist->id;
        $heistPlan->user_id = $user->id;
        $heistPlan->city_id = $user->city_id;
        $heistPlan->status = 'planned';
        $user->heistPlan = $heistPlan;

        //create HeistParticipations
        $heistParticipations = [];
        foreach($heist->heistRoles as $role)
        {
            $userId = $roles[$role->id];

            $participation = new HeistParticipation();
            $participation->heist_role_id = $role->id;
            $participation->user_id = $userId;
            $participation->status = $userId == $user->id ? 'accepted' : 'invited';

            $heistParticipations[] = $participation;
        }

        //create Invitations
        $invitations = [];
        foreach($heistParticipations as $participation)
        {
            if($participation->user->id != $user->id) {
                $invitations[] = $this->notificationService->create($participation->user, $user, 'heistPlan-invitation', [], $requestOid);
            }
        }

        //save models
        $heistPlan->save();
        $heistPlan->heistParticipations()->saveMany($heistParticipations);
        $heistPlan->invitations()->saveMany($invitations);

        //respond
        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser( $user ),
        ]);
    }

    /**
     * @param $heistPlanId
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($heistPlanId)
    {
        $user = Auth::user();
        $heistPlan = $user->heistPlan;

        //validation
        if($heistPlan === null || $heistPlan->id !== $heistPlanId) {
            return $this->respondInvalidRequest('Invalid heistPlanId');
        }

        //cancel plan
        $heistPlan->status = 'cancelled';
        $user->heistPlan = null;

        //save models
        $heistPlan->save();

        //send notifications
        foreach($heistPlan->heistParticipations as $participation)
        {
            if($participation->user->id != $user->id) {
                $unreadNotification = $heistPlan->invitations()->where('user_id', $participation->user->id)->where('read_at', null)->first();

                if($unreadNotification) {
                    $this->facebook->deleteRequest($unreadNotification->facebook_request_oid, $participation->user);
                    $unreadNotification->delete();
                }
                else {
                    $this->notificationService->create($participation->user, $user, 'heistPlan-cancelled', [
                        'heist_plan_id' => $heistPlan->id
                    ])->save();
                }
            }
        }

        //respond
        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser( $user ),
        ]);
    }
}