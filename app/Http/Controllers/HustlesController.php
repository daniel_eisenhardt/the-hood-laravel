<?php

namespace App\Http\Controllers;

use App\Models\Hustle;
use App\Services\Calculator;
use App\Services\UserService;
use App\Transformers\HustleTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Config;

class HustlesController extends ApiController
{

    protected $hustleTransformer;

    protected $userTransformer;

    protected $userService;

    protected $calculator;

    public function __construct(HustleTransformer $hustleTransformer, UserTransformer $userTransformer, UserService $userService, Calculator $calculator)
    {
        parent::__construct();

        $this->hustleTransformer = $hustleTransformer;
        $this->userTransformer = $userTransformer;
        $this->userService = $userService;
        $this->calculator = $calculator;
    }

    public function index()
    {
        $hustles = Hustle::all();

        return $this->respond($this->hustleTransformer->transformCollection( $hustles ));
    }

    public function execute($id)
    {
        $hustle = Hustle::findOrFail($id);
        $user = \Auth::user();
;
        if (strtotime($user->hustled_at) + Config::get('rules.hustle_time') > time()) {
            return $this->respondInvalidRequest('Cannot hustle yet!');
        }

        //determine result
        $level = ($this->userService->getLevel($user) + $this->userService->getAttributeLevel($user, $hustle->attribute)) / 2;
        $success = $this->calculator->randSuccess($level, $hustle->difficulty);
        $result = ['success' => $success];

        //store consequences
        $user->hustled_at = date('Y-m-d H:i:s');
        $user->last_hustle_id = $hustle->id;

        if($success) {
            $this->userService->addAttributeXp($user, $hustle->attribute, 1);
            $reward = mt_rand( ceil($hustle->money_max/2), $hustle->money_max );
            $user->money += $reward;
            $result['reward'] = $reward;
            $user->save();
        }
        else {
            $user->addHeatAndSave( $hustle->heat );
        }

        //respond
        return $this->respond([
            'result' => $result,
            'user' => $this->userTransformer->transformActiveUser($user)
        ]);
    }
}