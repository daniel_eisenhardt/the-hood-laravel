<?php


namespace App\Http\Controllers;

use App\Models\User;
use App\Services\Facebook;
use App\Services\NotificationService;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends ApiController
{
    /**
     * @var UserTransformer
     */
    protected $userTransformer;

    /**
     * @var NotificationService
     */
    protected $notificationService;

    /**
     * @var Facebook
     */
    protected $facebook;

    public function __construct(UserTransformer $userTransformer, NotificationService $notificationService, Facebook $facebook)
    {
        parent::__construct();

        $this->userTransformer = $userTransformer;
        $this->notificationService = $notificationService;
        $this->facebook = $facebook;
        $this->facebook->setTokenFromUser(Auth::user());
    }

    public function friends()
    {
        $friendIds = $this->facebook->getFriendIds();

        $friends = User::whereIn('facebook_id', $friendIds)->get();

        return $this->respond($this->userTransformer->transformCollection($friends));
    }

    public function show($id)
    {
        $user = User::where('id', $id)->first();

        return $this->respond($this->userTransformer->transform($user->toArray()));
    }

    public function sendmessage($receiverId)
    {
        $user = Auth::user();
        $receiver = User::findOrFail($receiverId);
        $message = Request::input('message');

        if(empty($message)) {
            return $this->respondInvalidRequest('Cannot send an empty message!');
        }

        //create notification
        $notification = $this->notificationService->create($receiver, $user, 'message', ['message' => $message]);

        //save
        $notification->save();

        //respond
        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser( \Auth::user() ),
        ]);
    }

    public function sendmoney($receiverId)
    {
        $user = Auth::user();
        $receiver = User::findOrFail($receiverId);
        $amount = Request::input('amount');
        $message = Request::input('message');

        if($amount <= 0) {
            return $this->respondInvalidRequest('Amount must be larger than 0!');
        }

        if($user->money < $amount) {
            return $this->respondInvalidRequest('Not enough money!');
        }

        //transfer money
        $user->money -= $amount;
        $receiver->money += $amount;

        //create notification
        $notification = $this->notificationService->create($receiver, $user, 'transaction', [
            'amount' => $amount,
            'message' => $message,
        ]);

        //save
        $user->save();
        $receiver->save();
        $notification->save();

        //respond
        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser( \Auth::user() ),
        ]);
    }
}