<?php


namespace App\Http\Controllers;


use App\Models\Notification;
use App\Services\Facebook;
use App\Transformers\NotificationTransformer;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends ApiController
{

    protected $notificationTransformer;

    protected $facebook;

    public function __construct(NotificationTransformer $notificationTransformer, Facebook $facebook)
    {
        parent::__construct();

        $this->notificationTransformer = $notificationTransformer;
        $this->facebook = $facebook;
        $this->facebook->setTokenFromUser(Auth::user());
    }

    public function markRead($id)
    {
        $notification = Notification::find($id);

        if($notification->user_id !== Auth::user()->id) {
            return $this->respondInvalidRequest('This notification belongs to a different user!');
        }

        $notification->read_at = date('Y-m-d H:i:s');

        if($notification->facebook_request_oid !== null) {
            $this->facebook->deleteRequest($notification->facebook_request_oid, Auth::user());
        }

        $notification->save();

        return $this->respond($this->notificationTransformer->transform($notification));
    }

}