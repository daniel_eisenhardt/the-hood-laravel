<?php


namespace App\Http\Controllers;


use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Config;

/**
 * Class BailController
 * @package App\Http\Controllers
 */
class BailController extends ApiController
{

    /**
     * @var UserTransformer
     */
    protected $userTransformer;

    /**
     * BailController constructor.
     * @param UserTransformer $userTransformer
     */
    public function __construct(UserTransformer $userTransformer)
    {
        parent::__construct();

        $this->userTransformer = $userTransformer;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function execute()
    {
        $user = \Auth::user();
        if($user->arrested_at === null) {
            return $this->respondInvalidRequest('Not in jail!');
        }

        $price = Config::get('rules.bailout_price') * pow(Config::get('rules.bailout_price_factor'), floor($user->reputation->heat/100)-1);
        if($price > $user->money) {
            return $this->respondInvalidRequest('Not enough money!');
        }

        $user->money -= $price;
        $user->arrested_at = null;
        $user->save();

        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser($user)
        ]);
    }
}