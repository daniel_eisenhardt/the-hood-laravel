<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Services\CityService;
use App\Transformers\CityTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Config;

/**
 * Class JobsController
 * @package App\Http\Controllers
 */
class CitiesController extends ApiController
{
    /**
     * @var CityTransformer
     */
    protected $cityTransformer;

    /**
     * @var CityService
     */
    protected $cityService;

    /**
     * @var UserTransformer
     */
    protected $userTransformer;

    /**
     * CitiesController constructor.
     * @param CityTransformer $cityTransformer
     * @param UserTransformer $userTransformer
     * @param CityService $cityService
     */
    public function __construct(CityTransformer $cityTransformer, UserTransformer $userTransformer, CityService $cityService)
    {
        parent::__construct();

        $this->cityTransformer = $cityTransformer;
        $this->userTransformer = $userTransformer;
        $this->cityService = $cityService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $cities = City::all();

        return $this->respond($this->cityTransformer->transformCollection( $cities ));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function travel($id)
    {
        $user = \Auth::user();
        $destination = City::findOrFail($id);

        if($user->travel_time !== null) {
            return $this->respondInvalidRequest('Cannot travel yet!');
        }

        $price = $this->cityService->getTicketPrice(\Auth::user()->city, $destination);
        if($price > $user->money) {
            return $this->respondInvalidRequest('Not enough money!');
        }

        //update origin reputation
        $user->reputation->left_at = date('Y-m-d H:i:s');
        $user->reputation->save();

        //update user
        $user->money -= $price;
        $user->city_id = $destination->id;
        $user->travel_time = date('Y-m-d H:i:s', time() + Config::get('rules.travel_time'));
        $user->save();

        //update destination reputation
        $user->reputation->heat = $user->reputation->heat;
        $user->reputation->left_at = null;
        $user->reputation->save();

        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser($user)
        ]);
    }
}