<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Config;

/**
 * Class AuthController
 * @package app\Http\Controllers
 */
class AuthController extends ApiController
{
    /**
     * @var \App\Services\User;
     */
    private $userService;

    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    private $JWTAuth;

    /**
     * @var \App\Transformers\UserTransformer
     */
    private $userTransformer;

    /**
     * AuthController constructor.
     * @param \App\Services\UserService $userService
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @param \App\Transformers\UserTransformer $userTransformer
     */
    public function __construct(\App\Services\UserService $userService, \Tymon\JWTAuth\JWTAuth $JWTAuth, \App\Transformers\UserTransformer $userTransformer)
    {
        parent::__construct();

        $this->userService = $userService;
        $this->JWTAuth = $JWTAuth;
        $this->userTransformer = $userTransformer;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function config()
    {
        return $this->respond([
            'time' => time(),
            'facebook_app_id' => Config::get('laravel-facebook-sdk.facebook_config.app_id'),
            'energy_max' => (int) Config::get('rules.energy_max'),
            'energy_time' => (int) Config::get('rules.energy_time'),
            'travel_price' => (int) Config::get('rules.travel_price'),
            'heat_time' => (int) Config::get('rules.heat_time'),
            'arrest_time' => (int) Config::get('rules.arrest_time'),
            'bailout_price' => (int) Config::get('rules.bailout_price'),
            'bailout_price_factor' => (int) Config::get('rules.bailout_price_factor'),
            'hustle_time' => (int) Config::get('rules.hustle_time'),
        ]);
    }

    /**
     * @param \App\Services\Facebook $facebook
     */
    public function login(\App\Services\Facebook $facebook)
    {
        $facebookToken = Request::input('facebook_token');

        try {
            $facebook->setToken($facebookToken);
            $graphUser = $facebook->getGraphUser();
        }
        catch (\Facebook\Exceptions\FacebookSDKException $e) {
            if($e instanceof \Facebook\Exceptions\FacebookResponseException) {
                return $this->respondInvalidRequest('facebook-session-expired');
            }
            return $this->respondInternalerror( $e->getMessage() );
        }

        $user = $this->userService->getUserFromGraphUser($graphUser);

        $this->userService->storeFacebookAccessToken($user, $facebook->getToken());

        $token = $this->JWTAuth->fromUser($user);

        return $this->respond([
            'access_token' => $token
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function user()
    {
        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser( \Auth::user() ),
        ]);
    }
}