<?php


namespace App\Http\Controllers;


use App\Services\Facebook;
use App\Services\HeistService;
use App\Services\NotificationService;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;

/**
 * Class HeistParticipationsController
 * @package App\Http\Controllers
 */
class HeistParticipationsController extends ApiController
{
    /**
     * @var UserTransformer
     */
    protected $userTransformer;

    /**
     * @var HeistService
     */
    protected $heistService;

    /**
     * @var NotificationService
     */
    protected $notificationService;

    /**
     * @var Facebook
     */
    protected $facebook;

    /**
     * HeistParticipationsController constructor.
     * @param UserTransformer $userTransformer
     * @param HeistService $heistService
     * @param Facebook $facebook
     */
    public function __construct(UserTransformer $userTransformer, HeistService $heistService, NotificationService $notificationService, Facebook $facebook)
    {
        parent::__construct();

        $this->userTransformer = $userTransformer;
        $this->heistService = $heistService;
        $this->notificationService = $notificationService;
        $this->facebook = $facebook;
        $this->facebook->setTokenFromUser(Auth::user());
    }

    /**
     * @param $heistParticipationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function accept($heistParticipationId)
    {
        //retrieve models
        $user = Auth::user();
        $heistParticipation = $user->heistParticipations()
            ->where('id', $heistParticipationId)
            ->with('heistPlan', 'heistPlan.heist')
            ->firstOrFail();

        //save changes
        $heistParticipation->status = 'accepted';
        $heistParticipation->save();

        //execute if everyone is ready
        if($this->heistService->isReady($heistParticipation->heistPlan)) {
            $result = $this->heistService->execute($heistParticipation->heistPlan);

            //create notifications
            foreach($heistParticipation->heistPlan->heistParticipations as $participation)
            {
                if($result['success']) {
                    $this->notificationService->create($participation->user, $user, 'heist-succeeded', [
                        'heist_plan_id' => $heistParticipation->heistPlan->id,
                        'reward' => $result['reward'],
                    ])->save();
                }
                else {
                    $this->notificationService->create($participation->user, $user, 'heist-failed', [
                        'heist_plan_id' => $heistParticipation->heistPlan->id,
                    ])->save();
                }
            }
        }

        //respond
        $user = $user->findOrFail( $user->id );
        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser( $user ),
        ]);
    }

    /**
     * @param $heistParticipationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function reject($heistParticipationId)
    {
        //retrieve models
        $user = Auth::user();
        $heistParticipation = $user->heistParticipations()
            ->where('id', $heistParticipationId)
            ->with('heistPlan', 'heistPlan.heist')
            ->firstOrFail();
        $heistPlan = $heistParticipation->heistPlan;

        //save changes
        $heistParticipation->status = 'rejected';
        $heistParticipation->save();

        $heistParticipation->heistPlan->status = 'cancelled';
        $heistParticipation->heistPlan->save();

        //create notifications
        foreach($heistPlan->heistParticipations as $participation)
        {
            if($participation->user->id != $user->id) {
                $unreadNotification = $heistPlan->invitations()->where('user_id', $participation->user->id)->where('read_at', null)->first();

                if($unreadNotification) {
                    $this->facebook->deleteRequest($unreadNotification->facebook_request_oid, $participation->user);
                    $unreadNotification->delete();
                }
                else {
                    $this->notificationService->create($participation->user, $user, 'heistPlan-cancelled', [
                        'heist_plan_id' => $heistPlan->id,
                        'action' => 'reject'
                    ])->save();
                }
            }
        }

        //respond
        $user = $user->findOrFail( $user->id );
        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser( $user ),
        ]);
    }

    /**
     * @param $heistParticipationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($heistParticipationId)
    {
        //retrieve models
        $user = Auth::user();
        $heistParticipation = $user->heistParticipations()
            ->where('id', $heistParticipationId)
            ->with('heistPlan', 'heistPlan.heist')
            ->firstOrFail();
        $heistPlan = $heistParticipation->heistPlan;

        //save changes
        $heistParticipation->status = 'cancelled';
        $heistParticipation->save();

        $heistParticipation->heistPlan->status = 'cancelled';
        $heistParticipation->heistPlan->save();

        //create notifications
        foreach($heistParticipation->heistPlan->heistParticipations as $participation)
        {
            if($participation->user->id != $user->id) {
                $unreadNotification = $heistPlan->invitations()->where('user_id', $participation->user->id)->where('read_at', null)->first();

                if($unreadNotification) {
                    $this->facebook->deleteRequest($unreadNotification->facebook_request_oid, $participation->user);
                    $unreadNotification->delete();
                }
                else {
                    $this->notificationService->create($participation->user, $user, 'heistPlan-cancelled', [
                        'heist_plan_id' => $heistPlan->id,
                        'action' => 'cancel'
                    ])->save();
                }
            }
        }

        //respond
        $user = $user->findOrFail( $user->id );
        return $this->respond([
            'user' => $this->userTransformer->transformActiveUser( $user ),
        ]);
    }
}