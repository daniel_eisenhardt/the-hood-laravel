<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Services\Calculator;
use App\Services\UserService;
use App\Transformers\JobTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Config;

/**
 * Class JobsController
 * @package App\Http\Controllers
 */
class JobsController extends ApiController
{

    /**
     * @var JobTransformer
     */
    protected $jobTransformer;

    /**
     * @var UserTransformer
     */
    protected $userTransformer;

    /**
     * @var Calculator
     */
    protected $calculator;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * JobsController constructor.
     * @param JobTransformer $jobTransformer
     * @param UserTransformer $userTransformer
     * @param Calculator $calculator
     * @param UserService $userService
     */
    public function __construct(JobTransformer $jobTransformer, UserTransformer $userTransformer, Calculator $calculator, UserService $userService)
    {
        parent::__construct();

        $this->jobTransformer = $jobTransformer;
        $this->userTransformer = $userTransformer;
        $this->calculator = $calculator;
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $jobs = Job::all();

        return $this->respond($this->jobTransformer->transformCollection( $jobs ));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function execute($id)
    {
        $job = Job::findOrFail($id);
        $user = \Auth::user();

        if((int) $user->energy < 1) {
            return $this->respondInvalidRequest('No energy!');
        }

        //determine result
        $level = $this->userService->getLevel($user);
        $success = $this->calculator->randSuccess($level, $job->difficulty);
        $result = ['success' => $success];

        //store consequences
        if($user->energy == Config::get('rules.energy_max')) {
            $user->energy_time = date('Y-m-d H:i:s');
        }
        $user->energy_time = $user->energy_time;
        $user->energy -= 1;
        $user->last_job_id = $job->id;
        $user->xp ++;

        if($success) {
            $reward = mt_rand( ceil($job->money_max/2), $job->money_max );
            $user->money += $reward;
            $result['reward'] = $reward;
            $user->save();
        }
        else {
            $user->addHeatAndSave( $job->heat );
        }

        //respond
        return $this->respond([
            'result' => $result,
            'user' => $this->userTransformer->transformActiveUser($user)
        ]);
    }
}