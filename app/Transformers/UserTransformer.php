<?php


namespace App\Transformers;


use App\Models\User;

/**
 * Class UserTransformer
 * @package App\Transformers
 */
class UserTransformer extends Transformer
{

    /**
     * @var ReputationTransformer
     */
    protected $reputationTransformer;

    /**
     * @var HeistPlanTransformer
     */
    protected $heistPlanTransformer;

    /**
     * UserTransformer constructor.
     * @param ReputationTransformer $reputationTransformer
     * @param HeistPlanTransformer $heistPlanTransformer
     */
    public function __construct(ReputationTransformer $reputationTransformer, HeistPlanTransformer $heistPlanTransformer)
    {
        $this->reputationTransformer = $reputationTransformer;
        $this->heistPlanTransformer = $heistPlanTransformer;
    }

    /**
     * @param $item
     * @return array
     */
    public function transform($item)
    {
        return [
            'id' => (int) $item['id'],
            'facebook_id' => $item['facebook_id'],
            'first_name' => $item['first_name'],
            'last_name' => empty($item['last_name']) ? 'X' : strtoupper(substr($item['last_name'], 0, 1)),
            'xp' => (int) $item['xp'],
            'cha_xp' => (int) $item['cha_xp'],
            'dex_xp' => (int) $item['dex_xp'],
            'int_xp' => (int) $item['int_xp'],
            'str_xp' => (int) $item['str_xp'],
        ];
    }

    /**
     * @param User $user
     * @return array
     */
    public function transformActiveUser(User $user)
    {
        return array_merge($this->transform($user->toArray()), [
            'money' => (int) $user->money,
            'energy' => (int) $user->energy,
            'energy_time' => $user->energy_time === null ? null : strtotime($user->energy_time),
            'city_id' => (int) $user->city_id,
            'travel_time' => $user->travel_time === null ? null : strtotime($user->travel_time),
            'reputations' => $this->reputationTransformer->transformCollection($user->reputations),
            'arrested_at' => $user->arrested_at === null ? null : strtotime($user->arrested_at),
            'hustled_at' => $user->hustled_at === null ? null : strtotime($user->hustled_at),
            'last_job_id' => $user->last_job_id === null ? null : (int) $user->last_job_id,
            'last_hustle_id' => $user->last_hustle_id === null ? null : (int) $user->last_hustle_id,
            'heist_plan' => $user->heistPlan === null ? null : $this->heistPlanTransformer->transform($user->heistPlan),
            'heist_plans' => $this->heistPlanTransformer->transformCollection($user->heistPlans),
        ]);
    }
}