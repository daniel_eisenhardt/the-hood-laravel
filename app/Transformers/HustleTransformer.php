<?php


namespace App\Transformers;


class HustleTransformer extends Transformer
{

    public function transform($item)
    {
        return [
            'id'         => (int) $item['id'],
            'name'       => $item['name'],
            'attribute'  => $item['attribute'],
            'difficulty' => (int) $item['difficulty'],
            'money_min'  => (int) ceil($item['money_max']/2),
            'money_max'  => (int) $item['money_max'],
            'heat'       => (int) $item['heat'],
        ];
    }
}