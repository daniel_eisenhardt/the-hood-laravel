<?php


namespace App\Transformers;


class HeistPlanTransformer extends Transformer
{

    protected $heistParticipationTransformer;

    public function __construct(HeistParticipationTransformer $heistParticipationTransformer)
    {
        $this->heistParticipationTransformer = $heistParticipationTransformer;
    }

    public function transform($item)
    {
        return [
            'id' => (int) $item['id'],
            'heist_id' => (int) $item['heist_id'],
            'user_id' => (int) $item['user_id'],
            'city_id' => (int) $item['city_id'],
            'status' => $item['status'],
            'money' => empty($item['money']) ? null : (int) $item['money'],
            'heist_participations' => $this->heistParticipationTransformer->transformCollection($item->heistParticipations),
        ];
    }
}