<?php


namespace App\Transformers;


class ReputationTransformer extends Transformer
{

    public function transform($item)
    {
        return [
            'user_id' => (int) $item['user_id'],
            'city_id' => (int) $item['city_id'],
            'heat' => (int) $item['heat'],
            'left_at' => $item['left_at'] === null ? null : strtotime($item['left_at']),
        ];
    }
}