<?php


namespace App\Transformers;


class NotificationTransformer extends Transformer
{

    public function transform($item)
    {
        return [
            'id' => (int) $item['id'],
            'user_id' => (int) $item['user_id'],
            'sender_id' => (int) $item['sender_id'],
            'type' => $item['type'],
            'data' => $item['data'],
            'read_at' => $item['read_at'] === null ? null : strtotime($item['read_at']),
            'heist_plan_id' => $item['heist_plan_id'],
        ];
    }
}