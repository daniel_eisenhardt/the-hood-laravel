<?php


namespace App\Transformers;


/**
 * Class HeistParticipationTransformer
 * @package App\Transformers
 */
class HeistParticipationTransformer extends Transformer
{

    /**
     * @param $item
     * @return array
     */
    public function transform($item)
    {
        return [
            'id' => (int) $item['id'],
            'heist_plan_id' => (int) $item['heist_plan_id'],
            'heist_role_id' => (int) $item['heist_role_id'],
            'user_id' => (int) $item['user_id'],
            'status' => $item['status'],
        ];
    }
}