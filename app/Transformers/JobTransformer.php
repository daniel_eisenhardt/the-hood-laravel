<?php

namespace App\Transformers;

/**
 * Class JobTransformer
 * @package App\Transformers
 */
class JobTransformer extends Transformer
{

    /**
     * @param $item
     * @return array
     */
    public function transform($item)
    {
        return [
            'id'         => (int) $item['id'],
            'name'       => $item['name'],
            'difficulty' => (int) $item['difficulty'],
            'money_min'  => (int) ceil($item['money_max']/2),
            'money_max'  => (int) $item['money_max'],
            'heat'       => (int) $item['heat'],
        ];
    }
}