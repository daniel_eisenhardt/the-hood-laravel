<?php


namespace App\Transformers;


/**
 * Class HeistTransformer
 * @package App\Transformers
 */
class HeistTransformer extends Transformer
{

    /**
     * @var HeistRoleTransformer
     */
    protected $heistRoleTransformer;

    /**
     * HeistTransformer constructor.
     * @param HeistRoleTransformer $heistRoleTransformer
     */
    public function __construct(HeistRoleTransformer $heistRoleTransformer)
    {
        $this->heistRoleTransformer = $heistRoleTransformer;
    }

    /**
     * @param $item
     * @return array
     */
    public function transform($item)
    {
        return [
            'id'          => (int) $item['id'],
            'name'        => $item['name'],
            'difficulty'  => (int) $item['difficulty'],
            'money_min'   => (int) ceil($item['money_max']/2),
            'money_max'   => (int) $item['money_max'],
            'heat'        => (int) $item['heat'],
            'heist_roles' => $this->heistRoleTransformer->transformCollection($item->heistRoles)
        ];
    }
}