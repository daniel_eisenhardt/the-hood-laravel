<?php


namespace App\Transformers;


class HeistRoleTransformer extends Transformer
{

    public function transform($item)
    {
        return [
            'id'        => (int) $item['id'],
            'name'      => $item['name'],
            'attribute' => $item['attribute']
        ];
    }
}