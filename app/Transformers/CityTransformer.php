<?php


namespace App\Transformers;


/**
 * Class CityTransformer
 * @package App\Transformers
 */
class CityTransformer extends Transformer
{

    /**
     * @param $item
     * @return array
     */
    public function transform($item)
    {
        return [
            'id' => (int) $item['id'],
            'name' => $item['name'],
            'x' => (int) $item['x'],
            'y' => (int) $item['y'],
        ];
    }
}