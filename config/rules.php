<?php

return [
    'energy_max' => 30,
    'energy_time' => env('ENERGY_TIME', 900), //15 minutes
    'travel_price' => 3,
    'travel_time' => env('TRAVEL_TIME', 7200), //2 hours
    'heat_time' => env('HEAT_TIME', 180), //3 minutes
    'arrest_time' => env('ARREST_TIME', 3600), //1 hour
    'bailout_price' => 100,
    'bailout_price_factor' => 10,
    'hustle_time' => env('HUSTLE_TIME', 30),
];