'use strict';

/**
 * @ngdoc directive
 * @name publicApp.directive:level
 * @description
 * # level
 */
angular.module('publicApp')
    .directive('level', function () {
        return {
            template: '{{level}}',
            restrict: 'E',
            scope: {
                xp: '='
            },
            controller: ['$scope', 'calculatorService', function($scope, calculator) {

                $scope.level = calculator.getLevel($scope.xp);
            }]
        };
    });
