'use strict';

/**
 * @ngdoc directive
 * @name publicApp.directive:heistPlanChance
 * @description
 * # heistPlanChance
 */
angular.module('publicApp')
    .directive('heistPlanChance', function () {
        return {
            template: '<span>{{chance}}</span>',
            restrict: 'A',
            scope: {
                'plan': '='
            },
            controller: ['$scope', 'calculatorService', function($scope, calculator) {

                $scope.$watch('plan.version', function() {
                    var powers = [];
                    var difficulty = $scope.plan.heist.difficulty;

                    angular.forEach($scope.plan.heist.heist_roles, function(role) {

                        var participant = $scope.plan.participants[ role.id ];

                        powers.push( calculator.getLevel(participant.xp) );

                        switch(role.attribute) {
                            case 'cha':
                                powers.push( calculator.getLevel(participant.cha_xp) );
                                break;
                            case 'dex':
                                powers.push( calculator.getLevel(participant.dex_xp) );
                                break;
                            case 'int':
                                powers.push( calculator.getLevel(participant.int_xp) );
                                break;
                            case 'str':
                                powers.push( calculator.getLevel(participant.str_xp) );
                                break;
                        }
                    });

                    var power = powers.reduce(function(a, b) { return a + b; }) / powers.length;

                    $scope.chance = Math.round(calculator.getSuccessChance(power, difficulty)*100);
                });
            }]
        };
    });
