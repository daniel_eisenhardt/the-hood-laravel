'use strict';

/**
 * @ngdoc filter
 * @name publicApp.filter:secondsToDateTime
 * @function
 * @description
 * # secondsToDateTime
 * Filter in the publicApp.
 */
angular.module('publicApp')
    .filter('secondsToDateTime', function () {
        return function (seconds) {
            return new Date(1970, 0, 1).setSeconds(seconds);
        };
    });
