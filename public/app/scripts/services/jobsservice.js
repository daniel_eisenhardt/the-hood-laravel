'use strict';

/**
 * @ngdoc service
 * @name publicApp.jobsService
 * @description
 * # jobsService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('jobsService', ['$resource', '$http', function ($resource, $http) {
        var resource = $resource(
            '/api/1.0/jobs',
            null,
            {
                'query': {method:'GET', isArray: true, cache: true}
            }
        );

        resource.lastResult = {};
        resource.execute = function(id) {
            return $http.post('/api/1.0/jobs/' + id + '/execute')
                .success(function(response) {
                    angular.extend(resource.lastResult, response.result);
                });
        };

        return resource;
    }]);
