'use strict';

/**
 * @ngdoc service
 * @name publicApp.configservice
 * @description
 * # configservice
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('configService', ['$http', '$q', function ($http, $q) {

        var _this = this;
        var data = {};
        var initPromise;
        var serverTimeDelta = 0;

        (function() { //constructor logic
            var deferred = $q.defer();

            $http({
                url: '/api/1.0/auth/config',
                skipAuthorization: true,
                method: 'GET'
            }).then(function (response) {
                data = response.data;

                deferred.resolve(response.data);
                serverTimeDelta = Math.floor(new Date().getTime()/1000) - response.data.time;
            });

            initPromise = deferred.promise;
        }());

        this.getAfterLoad = function(key) {
            return initPromise.then(function() {
                return _this.get(key);
            });
        };

        this.get = function(key) {
            if(typeof data[key] === 'undefined') {
                return null;
            }
            return data[key];
        };

        this.time = function() {
            return Math.floor(new Date().getTime()/1000) - serverTimeDelta;
        };
    }]);
