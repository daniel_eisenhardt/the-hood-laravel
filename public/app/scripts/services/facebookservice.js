'use strict';

/**
 * @ngdoc service
 * @name publicApp.facebookService
 * @description
 * # facebookService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('facebookService', ['$q', 'configService', function ($q, config) {

        var sdk = FB;
        var initPromise;
        var scope = ['public_profile', 'user_friends'];

        (function() { //constructor logic
            var deferred = $q.defer();

            config.getAfterLoad('facebook_app_id').then(function (facebook_app_id) {
                sdk.init({
                    appId: facebook_app_id,
                    status: true,
                    cookie: true,
                    xfbml: true
                });

                var login = function(){
                    sdk.login(function (response) {
                        if (response.status === 'connected') {
                            deferred.resolve(response.authResponse.accessToken);
                        }
                        else {
                            deferred.reject();
                        }
                    }, {scope: scope.join(',')});
                };


                sdk.getLoginStatus(function (response) {

                    if (response.status === 'connected') {
                        sdk.api('/me/permissions', function(response) {
                            var granted;

                            angular.forEach(scope, function(permission) {
                                granted = false;
                                for(var i in response.data) {

                                    if(response.data[i]['permission'] === permission && response.data[i]['status'] === 'granted') {
                                        granted = true;
                                    }
                                }

                                if(granted === false) {
                                    login();
                                    return false;
                                }

                            });
                        });
                        deferred.resolve(response.authResponse.accessToken);
                    }
                    else {
                        login();
                    }
                });
            });

            initPromise = deferred.promise;
        }());


        this.login = function () {
            return initPromise;
        };

        this.sendRequests = function(ids, message) {

            var deferred = $q.defer();

            sdk.ui({
                method: 'apprequests',
                message: message,
                to: ids.join(',')
            }, function(response) {
                if(typeof response.request !== 'undefined') {
                    deferred.resolve(response.request);
                } else {
                    deferred.reject();
                }
            });

            return deferred.promise;
        };

        this.sendInvitiations = function() {

            sdk.ui({
                method: 'apprequests',
                message: 'Join me in the hood'
            });
        };

    }]);
