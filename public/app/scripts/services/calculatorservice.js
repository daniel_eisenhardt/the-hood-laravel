'use strict';

/**
 * @ngdoc service
 * @name publicApp.calculatorService
 * @description
 * # calculatorService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('calculatorService', function () {

        var _this = this;

        this.getLevel = function(xp) {
            var level = 1;
            while(_this.getXpForLevel(level) <= xp) {
                level ++;
            }
            return level-1;
        };

        this.getXpForLevel = function(level) {
            var base = 15;

            base *= 75/83; //make the base the level 1 xp
            var outcome = Math.floor((base * Math.pow(2, level/7)) / (Math.pow(2, 1/7) -1) + (1/8)*Math.pow(level,2) - level/8 - (base*2)/(2-Math.pow(2, (6/7))));

            return Math.floor(outcome);
        };

        this.getCurrentXp = function(xp) {
            return xp - this.getXpForLevel(this.getLevel(xp));
        };

        this.getNextXp = function(xp) {
            return this.getXpForLevel(this.getLevel(xp)+1) - this.getXpForLevel(this.getLevel(xp));
        };

        this.getXpPercentage = function(xp) {
            return parseInt(this.getCurrentXp(xp) / this.getNextXp(xp) * 100);
        }

        this.getSuccessChance = function(power, difficulty) {
            power = Math.pow(power,2);
            difficulty = Math.pow(difficulty,2);
            return power / (power+difficulty);
        };

    });
