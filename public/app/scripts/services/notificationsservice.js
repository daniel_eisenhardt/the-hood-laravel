'use strict';

/**
 * @ngdoc service
 * @name publicApp.notificationsService
 * @description
 * # notificationsService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('notificationsService', ['$resource', '$http', function ($resource, $http) {

        var resource = $resource('/api/1.0/notifications');

        resource.markRead = function(id) {
            return $http.post('/api/1.0/notifications/' + id + '/markread');
        };

        return resource;
    }]);
