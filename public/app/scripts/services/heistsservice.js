'use strict';

/**
 * @ngdoc service
 * @name publicApp.heistsService
 * @description
 * # heistsService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('heistsService', ['$resource', '$http', function ($resource, $http) {

        var resource = $resource(
            '/api/1.0/heists',
            null,
            {
                'query': {method:'GET', isArray: true, cache: true},
                'get': {url: '/api/1.0/heists/:id'}
            }
        );

        resource.plan = function(id, roles, requestOid) {
            return $http.post('/api/1.0/heist_plans', {heist_id: id, roles: roles, request_oid: requestOid});
        };

        resource.cancel = function(heistPlanId) {
            return $http.post('/api/1.0/heist_plans/' + heistPlanId + '/cancel');
        };

        resource.getPlan = function(heistPlanId) {
            return $http.get('/api/1.0/heist_plans/' + heistPlanId);
        }

        resource.acceptParticipation = function(heistParticipationId) {
            return $http.post('/api/1.0/heist_participations/' + heistParticipationId + '/accept');
        };

        resource.rejectParticipation = function(heistParticipationId) {
            return $http.post('/api/1.0/heist_participations/' + heistParticipationId + '/reject');
        };

        resource.cancelParticipation = function(heistParticipationId) {
            return $http.post('/api/1.0/heist_participations/' + heistParticipationId + '/cancel');
        };

        return resource;
    }]);
