'use strict';

/**
 * @ngdoc service
 * @name publicApp.hustlesService
 * @description
 * # hustlesService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('hustlesService', ['$resource', '$http', function ($resource, $http) {
        var resource = $resource(
            '/api/1.0/hustles',
            null,
            {
                'query': {method:'GET', isArray: true, cache: true}
            }
        );

        resource.lastResult = {};
        resource.execute = function(id) {
            return $http.post('/api/1.0/hustles/' + id + '/execute')
                .success(function(response) {
                    angular.extend(resource.lastResult, response.result);
                });
        };

        return resource;
    }]);
