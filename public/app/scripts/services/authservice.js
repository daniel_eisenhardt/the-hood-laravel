'use strict';

/**
 * @ngdoc service
 * @name publicApp.authService
 * @description
 * # authService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('authService', ['$q', '$http', '$rootScope', 'facebookService', function ($q, $http, $rootScope, facebook) {
        var _this = this;
        var loginPromise;

        $rootScope.authStatus = 'pending-fb-auth';

        this.login = function () {
            if(typeof loginPromise === 'object' && loginPromise.$$state.status === 0) {
                return loginPromise; //prevent multiple simultaneous login requests
            }

            var deferred = $q.defer();

            var facebookPromise = facebook.login();

            facebookPromise.then(function (facebookToken) {
                $rootScope.authStatus = 'pending-api-auth';

                $http({
                    url: '/api/1.0/auth/login',
                    skipAuthorization: true,
                    method: 'POST',
                    data: {
                        'facebook_token': facebookToken
                    }
                }).then(function (response) {
                    _this.status = 'authenticated';
                    $rootScope.authStatus = 'authenticated';

                    deferred.resolve(response.data.access_token);
                });
            });

            facebookPromise.catch(function () {
                _this.status = 'rejected-facebook-auth';
                deferred.reject('rejected-facebook-auth');
            });

            loginPromise = deferred.promise;
            return deferred.promise;
        };

        this.setStatus = function(newStatus) {
            $rootScope.authStatus = newStatus;
        };

    }]);
