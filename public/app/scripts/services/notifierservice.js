'use strict';

/**
 * @ngdoc service
 * @name publicApp.notifierService
 * @description
 * # notifierService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('notifierService', ['$rootScope', '$uibModal', '$q', 'usersService', 'heistsService', 'notificationsService', function ($rootScope, $modal, $q, usersService, heistsService, notificationsService) {

        var _this = this;
        var notifications = {};
        var current = null;

        this.push = function (records) {
            angular.forEach(records, function(notification) {
                if(typeof notifications[notification.id] === 'undefined' || notification.id === null) {
                    notifications[notification.id] = notification;
                }
            });

            notify();
        }

        var notify = function() {
            if(current !== null) {
                return;
            }

            Object.keys(notifications).sort().some(function(key) {
                if(notifications[key].read_at === null) {
                    show(notifications[key])
                    return true;
                }
            });
        }

        var show = function(notification) {

            var promise = $q.when(true);
            var modalConfig = {scope: $rootScope.$new()};

            current = notification;

            if(notification.sender_id !== null) {
                promise = usersService.get({id: notification.sender_id}, function(user) {
                    modalConfig.scope.sender = user;
                }).$promise;
            }

            switch (notification.type) {

                case 'message':
                    modalConfig.templateUrl = '/app/views/modals/notifications/message.html';
                    modalConfig.scope.message = notification.data.message;
                    break;

                case 'transaction':
                    modalConfig.templateUrl = '/app/views/modals/notifications/transaction.html';
                    modalConfig.scope.amount = notification.data.amount;
                    modalConfig.scope.message = notification.data.message;
                    break;

                case 'heistPlan-invitation':
                    modalConfig.templateUrl = '/app/views/modals/notifications/heistplan_invitation.html';
                    promise = promise.then(function() {
                        return heistsService.getPlan(notification.heist_plan_id).success(function(response) {
                            modalConfig.scope.heistPlan = response;
                        });
                    });
                    modalConfig.controller = 'HeistplanCtrl';
                    break;

                case 'heistPlan-cancelled':
                    modalConfig.templateUrl = '/app/views/modals/notifications/heistplan_cancelled.html';
                    promise = promise.then(function() {
                        return heistsService.getPlan(notification.data.heist_plan_id).success(function(response) {
                            modalConfig.scope.heistPlan = response;
                        });
                    });
                    modalConfig.scope.action = notification.data.action;
                    modalConfig.controller = 'HeistplanCtrl';
                    break;

                case 'heist-succeeded':
                    modalConfig.templateUrl = '/app/views/modals/notifications/heist_succeeded.html';
                    promise = promise.then(function() {
                        return heistsService.getPlan(notification.data.heist_plan_id).success(function(response) {
                            modalConfig.scope.heistPlan = response;
                        });
                    });
                    modalConfig.scope.reward = notification.data.reward;
                    modalConfig.controller = 'HeistplanCtrl';
                    break;

                case 'heist-failed':
                    modalConfig.templateUrl = '/app/views/modals/notifications/heist_failed.html';
                    promise = promise.then(function() {
                        return heistsService.getPlan(notification.data.heist_plan_id).success(function(response) {
                            modalConfig.scope.heistPlan = response;
                        });
                    });
                    modalConfig.controller = 'HeistplanCtrl';
                    break;

                case 'level-up':
                    modalConfig.templateUrl = '/app/views/modals/notifications/level_up.html';
                    modalConfig.controller = 'LevelupCtrl';
                    modalConfig.scope.newLevel = notification.data.newLevel;
                    modalConfig.scope.attribute = notification.data.attribute;
                    break;

            }

            var modalPromise = promise.then(function () {
                return $modal.open(modalConfig).result;
            });

            modalPromise.finally(function() {
                notifications[notification.id].read_at = true;
                if(notification.id !== null) {
                    notificationsService.markRead(current.id);
                }

                current = null;
                notify();
            });
        };

    }]);
