'use strict';

/**
 * @ngdoc service
 * @name publicApp.responseDataInterceptor
 * @description
 * # responseDataInterceptor
 * Provider in the publicApp.
 */
angular.module('publicApp')
    .provider('responseDataInterceptor', function () {

        function ResponseDataInterceptor($injector) {

            this.response = function (response) {

                var notifier = $injector.get('notifierService');

                if (response.config.url.substring(0, 4) == "/api") {
                    notifier.push(response.data.notifications);

                    response.data = response.data.response_data;
                }

                return response;
            };
        }

        this.$get = ['$injector', function ($injector) {
            return new ResponseDataInterceptor($injector);
        }];
    });
