'use strict';

/**
 * @ngdoc service
 * @name publicApp.usersService
 * @description
 * # usersService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('usersService', ['$resource', '$http', function ($resource, $http) {

        var resource = $resource(
            '/api/1.0/users/:id',
            null,
            {
                'friends': {url: '/api/1.0/friends', method:'GET', isArray: true, cache: true},
                'get': {cache: true}
            }
        );

        resource.sendmessage = function(id, message) {
            return $http.post('/api/1.0/users/' + id + '/sendmessage', {message: message});
        };

        resource.sendmoney = function(id, amount, message) {
            return $http.post('/api/1.0/users/' + id + '/sendmoney', {amount: amount, message: message});
        };

        return resource;

    }]);
