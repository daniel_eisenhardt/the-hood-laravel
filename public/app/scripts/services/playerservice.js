'use strict';

/**
 * @ngdoc service
 * @name publicApp.playerService
 * @description
 * # playerService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('playerService', ['$q', '$http', 'configService', 'calculatorService', function ($q, $http, config, calculator) {
        var _this = this;
        this.data = {};

        this.setData = function(data) {
            angular.extend(_this.data, data);
        };

        this.load = function() {
            var deferred = $q.defer();

            if ( Object.keys(this.data).length === 0) {
                $http.get('/api/1.0/auth/user').then(function(response) {
                    _this.setData(response.data.user);
                    deferred.resolve();
                });
            } else {
                deferred.resolve();
            }

            return deferred.promise;
        };

        this.bailOut = function() {
            $http.post('/api/1.0/bailout').then(function(response) {
                _this.setData(response.data.user);
            });
        };

        this.getUser = function() {
            return {
                id: this.data['id'],
                facebook_id: this.data['facebook_id'],
                first_name: this.data['first_name'],
                last_name: this.data['last_name'],
                xp: this.data['xp'],
                cha_xp: this.data['cha_xp'],
                dex_xp: this.data['dex_xp'],
                int_xp: this.data['int_xp'],
                str_xp: this.data['str_xp'],
            };
        };

        this.getId = function() {
            return this.data['id'];
        };

        this.getLevel = function() {
            return calculator.getLevel(this.data['xp']);
        };

        this.getAttributeLevel = function(attribute) {
            return calculator.getLevel(getAttributeXp(attribute));
        };

        this.getXp = function() {
            return calculator.getCurrentXp(this.data['xp']);
        };

        this.getAttributeXp = function(attribute) {
            return calculator.getCurrentXp(getAttributeXp(attribute));
        }

        this.getXpTarget = function() {
            return calculator.getNextXp(this.data['xp']);
        };

        this.getAttributeXpTarget = function(attribute) {
            return calculator.getNextXp(getAttributeXp(attribute));
        };

        this.getXpPercentage = function() {
            return calculator.getXpPercentage(this.data['xp']);
        };

        this.getAttributeXpPercentage = function(attribute) {
            return calculator.getXpPercentage(getAttributeXp(attribute));
        };

        this.getMoney = function() {
            return this.data['money'];
        };

        this.getEnergy = function() {
            var energy = this.data['energy'];
            var seconds = config.time() - this.data['energy_time'];

            energy += Math.floor(seconds / config.get('energy_time'));

            energy = Math.min(energy, config.get('energy_max'));

            return energy;
        };

        this.getEnergyPercentage = function() {
            return parseInt(_this.getEnergy() / 30 * 100);
        };

        this.getEnergyTime = function() {
            if(_this.getEnergy() === 30) {
                return null;
            }

            var seconds = config.time() - this.data['energy_time'];

            seconds -= Math.floor(seconds / config.get('energy_time')) * config.get('energy_time');

            return config.get('energy_time') - seconds;
        };

        this.getCityId = function() {
            return this.data['city_id'];
        };

        this.getTravelTime = function() {
            var seconds = this.data['travel_time'] - config.time();

            return seconds > 0 ? seconds : null;
        };

        this.getHeat = function(city) {
            return getTotalHeat(city) - this.getHeatLevel(city)*100;
        };

        this.getHeatLevel = function(city) {
            return Math.floor(getTotalHeat(city)/100);
        };

        this.getHeatPercentage = function(city) {
            return parseInt(_this.getHeat(city) / 100 * 100);
        };

        this.getArrestedTime = function() {
            var time = null;

            if(this.data['arrested_at'] !== null) {
                time = this.data['arrested_at'] + config.get('arrest_time') - config.time();
            }

            return time > 0 ? time : null;
        };

        this.isArrested = function() {
            return _this.getArrestedTime() !== null;
        };

        this.getBailPrice = function() {
            return config.get('bailout_price') * Math.pow(config.get('bailout_price_factor'), (_this.getHeatLevel()-1));
        };

        this.getHustleTime = function() {
            var time = null;

            if(this.data['hustled_at'] !== null) {
                time = this.data['hustled_at'] + config.get('hustle_time') - config.time();
            }

            return time > 0 ? time : null;
        };

        this.getLastJobId = function() {
            return this.data['last_job_id'];
        };

        this.getLastHustleId = function() {
            return this.data['last_hustle_id'];
        };

        this.getHeistPlan = function() {
            return this.data['heist_plan'];
        };

        this.getHeistPlans = function() {
            return this.data['heist_plans'];
        };

        var getTotalHeat = function(city) {
            var cityId = typeof city === 'undefined' ? _this.getCityId() : city.id;
            var heat = 0;


            for(var i in _this.data['reputations']) { var reputation = _this.data['reputations'][i];
                if(reputation['city_id'] === cityId) {
                    heat = reputation['heat'];

                    if(reputation['left_at'] !== null) {
                        heat -= Math.floor((new Date().getTime()/1000 - reputation['left_at']) / config.get('heat_time'));
                    }

                    break;
                }
            }

            return Math.max(heat, 0);
        };

        var getAttributeXp = function(attribute) {
            switch (attribute) {
                case 'cha':
                    return _this.data['cha_xp'];
                    break;
                case 'dex':
                    return _this.data['dex_xp'];
                    break;
                case 'int':
                    return _this.data['int_xp'];
                    break;
                case 'str':
                    return _this.data['str_xp'];
                    break;
            }
        };
    }]);
