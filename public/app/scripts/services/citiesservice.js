'use strict';

/**
 * @ngdoc service
 * @name publicApp.citiesService
 * @description
 * # citiesService
 * Service in the publicApp.
 */
angular.module('publicApp')
    .service('citiesService', ['$resource', '$http', 'configService', function ($resource, $http, config) {
        var resource = $resource(
            '/api/1.0/cities',
            null,
            {
                'query': {method:'GET', isArray: true, cache: true}
            }
        );

        resource.getTicketPrice = function(origin, destination) {
            var distance = Math.ceil(Math.sqrt( //Pythagoras
                Math.pow(Math.abs( origin.x - destination.x), 2) +
                Math.pow(Math.abs( origin.y - destination.y), 2)
            ));

            return distance * config.get('travel_price');
        };

        resource.travel = function(destination) {
            return $http.post('/api/1.0/cities/' + destination.id + '/travel');
        };

        return resource;
    }]);
