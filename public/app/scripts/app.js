'use strict';

/**
 * @ngdoc overview
 * @name publicApp
 * @description
 * # publicApp
 *
 * Main module of the application.
 */
angular
    .module('publicApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'angular-jwt',
        'ui.bootstrap'
    ])
    .config(function ($routeProvider, $httpProvider, responseDataInterceptorProvider, jwtInterceptorProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/jobs.html',
                controller: 'JobsCtrl',
                controllerAs: 'jobs'
            })
            .when('/hustles', {
                templateUrl: 'views/hustles.html',
                controller: 'HustlesCtrl',
                controllerAs: 'hustles'
            })
            .when('/heists', {
                templateUrl: 'views/heists.html',
                controller: 'HeistsCtrl',
                controllerAs: 'heists'
            })
            .when('/travel', {
                templateUrl: 'views/travel.html',
                controller: 'TravelCtrl',
                controllerAs: 'travel'
            })
            .when('/profile', {
                templateUrl: 'views/profile.html',
                controller: 'ProfileCtrl',
                controllerAs: 'profile'
            })
            .otherwise({
                redirectTo: '/'
            });

        $httpProvider.interceptors.push('responseDataInterceptor');

        jwtInterceptorProvider.tokenGetter = ['jwtHelper', '$http', 'authService', function(jwtHelper, $http, auth) {

            var idToken = localStorage.getItem('id_token');
            var refreshToken = localStorage.getItem('refresh_token');

            if (idToken === null || jwtHelper.isTokenExpired(idToken)) {
                return auth.login().then(function(token) {
                    localStorage.setItem('id_token', token);
                    return token;
                });
            } else {
                auth.setStatus('authenticated');

                return idToken;
            }

        }];
        $httpProvider.interceptors.push('jwtInterceptor');
    })
    .run(['$rootScope', 'playerService', function($rootScope, player) {
        player.load();

        $rootScope.player = player;
    }]);
