'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('HeaderCtrl', ['$scope', '$interval', '$location', 'playerService', 'notifierService', function ($scope, $interval, $location, player, notifier) {

        $scope.$location = $location;
        $scope.energyTime = player.getEnergyTime();

        $interval(function() {
            $scope.energyTime = player.getEnergyTime();
        }, 1000);

        //this generates levelup notification modals
        $scope.$watch('player.getLevel()', function(newVal, oldVal) {
            showLevelup(oldVal, null);
        });

        $scope.$watch('player.getAttributeLevel("cha")', function(newVal, oldVal) {
            showLevelup(oldVal, 'cha');
        });

        $scope.$watch('player.getAttributeLevel("dex")', function(newVal, oldVal) {
            showLevelup(oldVal, 'dex');
        });

        $scope.$watch('player.getAttributeLevel("int")', function(newVal, oldVal) {
            showLevelup(oldVal, 'int');
        });

        $scope.$watch('player.getAttributeLevel("str")', function(newVal, oldVal) {
            showLevelup(oldVal, 'str');
        });

        var showLevelup = function(oldLevel, attribute) {

            if(oldLevel == 0) {
                return; //application not loaded yet
            }

            notifier.push([{
                id: null,
                type: 'level-up',
                read_at: null,
                sender_id: null,
                data: {
                    newLevel: oldLevel+1,
                    attribute: attribute
                }
            }]);
        }

    }]);
