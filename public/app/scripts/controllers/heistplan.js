'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:HeistplanCtrl
 * @description
 * # HeistplanCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('HeistplanCtrl', ['$scope', '$q', 'heistsService', 'usersService', 'playerService', 'calculatorService', function($scope, $q, heistsService, usersService, player, calculator) {

        $scope.player = player;
        $scope.loading = true;

        var playerParticipation;

        (function() { //constructor logic
            var plan = $scope.heistPlan;
            var promises = [];

            promises.push(
                usersService.get({id: plan.user_id}, function(user) {
                    plan.user = user;
                }).$promise
            );

            promises.push(
                heistsService.get({id: plan.heist_id}, function(heist) {
                    plan.heist = heist;
                }).$promise
            );

            plan.participants = {};
            angular.forEach(plan.heist_participations, function(participation) {

                if(participation.user_id === player.getId()) {
                    plan.participants[ participation.heist_role_id ] = player.getUser();
                    playerParticipation = participation;
                    $scope.playerStatus = participation.status;
                }
                else {
                    plan.participants[ participation.heist_role_id ] = {};
                    promises.push(
                        usersService.get({id: participation.user_id}, function(user) {
                            angular.extend(plan.participants[ participation.heist_role_id ], user);
                        }).$promise
                    );
                }
            });

            $q.all(promises).then(function() {
                $scope.loading = false;
            });
        }());

        $scope.getLevel = function (xp) {
            return calculator.getLevel(xp);
        };

        $scope.accept = function() {
            heistsService.acceptParticipation(playerParticipation.id).then(function(response) {
                player.setData(response.data.user);
            });
        };

        $scope.reject = function() {
            heistsService.rejectParticipation(playerParticipation.id).then(function(response) {
                player.setData(response.data.user);
            });
        };

        $scope.cancel = function() {
            heistsService.cancelParticipation(playerParticipation.id).then(function(response) {
                player.setData(response.data.user);
            });
        };

    }]);
