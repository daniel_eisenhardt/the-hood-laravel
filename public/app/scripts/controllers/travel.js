'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:TravelCtrl
 * @description
 * # TravelCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('TravelCtrl', ['$scope', 'citiesService', 'playerService', function ($scope, citiesService, player) {

        $scope.player = player;
        $scope.cities = [];
        $scope.selected;
        $scope.current;

        var cities = citiesService.query(function() {
            $scope.cities = cities;
            for(var i in cities) {
                if(cities[i].id == player.getCityId()) {
                    $scope.selected = cities[i];
                    $scope.current = cities[i];
                }
            }
        });

        $scope.select = function(city) {
            $scope.selected = city;
        };

        $scope.getTicketPrice = function(city) {
            return citiesService.getTicketPrice($scope.current, city);
        };

        $scope.canAffordTicket = function(city) {
            return $scope.getTicketPrice(city) <= player.getMoney();
        };

        $scope.travelTo = function(city) {
            citiesService.travel(city).then(function(response) {
                player.setData(response.data.user);
                $scope.current = $scope.selected;
            });
        };

    }]);
