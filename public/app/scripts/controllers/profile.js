'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('ProfileCtrl', ['$scope', '$uibModal', 'playerService', 'usersService', 'calculatorService', 'facebookService',
    function ($scope, $modal, player, usersService, calculator, facebook) {

        $scope.friends = null;

        (function() { //constructor logic

            var friends = usersService.friends(function() {
                $scope.friends = friends;
            });

        }());

        $scope.getLevel = function (xp) {
            return calculator.getLevel(xp);
        };

        $scope.showSendMoney = function(user) {
            var scope = $scope.$new();
            scope.user = user;

            var modal = $modal.open({
                templateUrl: '/app/views/modals/sendmoney.html',
                scope: scope
            });

            modal.result.then(function (result) {
                usersService.sendmoney(user.id, result.amount, result.message).then(function(response) {
                    player.setData(response.data.user);
                });
            });
        };

        $scope.showSendMessage = function(user) {
            var scope = $scope.$new();
            scope.user = user;

            var modal = $modal.open({
                templateUrl: '/app/views/modals/sendmessage.html',
                scope: scope
            });

            modal.result.then(function (message) {
                usersService.sendmessage(user.id, message).then(function(response) {
                    player.setData(response.data.user);
                });
            });
        };

        $scope.inviteFriends = function() {
            facebook.sendInvitiations();
        }

    }]);
