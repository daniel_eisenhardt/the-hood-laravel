'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('JobsCtrl', ['$scope', '$interval', '$q', 'jobsService', 'playerService', 'calculatorService', function ($scope, $interval, $q, jobsService, player, calculator) {

        $scope.jobs = [];
        $scope.selected = {job: null};
        $scope.progress = 0;
        $scope.lastResult = jobsService.lastResult;
        $scope.player = player;

        var jobs = jobsService.query(function() {
            $scope.jobs = jobs;
            $scope.selected.job = jobs[0];
            for(var i in jobs) {
                if(jobs[i].id === player.getLastJobId()) {
                    $scope.selected.job = jobs[i];
                }
            }
        });

        $scope.getChance = function(job) {
            return Math.floor( calculator.getSuccessChance(player.getLevel(), job['difficulty']) * 100 );
        };

        $scope.execute = function() {
            var apiPromise = jobsService.execute($scope.selected.job['id']);

            var timedPromise = $interval(function() {
                $scope.progress += 1;
            }, 10, 100);

            $q.all([apiPromise, timedPromise]).then(function(responses) {
                var response = responses[0];

                $scope.progress = 0;
                player.setData(response.data.user);
            });
        };

    }]);
