'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:LevelupCtrl
 * @description
 * # LevelupCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('LevelupCtrl', ['$scope', '$interval', '$timeout', 'calculatorService', function ($scope, $interval, $timeout, calculator) {

        $scope.level = $scope.newLevel-1;
        $scope.xpTarget = calculator.getXpForLevel($scope.newLevel) - calculator.getXpForLevel($scope.newLevel-1);
        $scope.xp = $scope.xpTarget-1;

        $scope.xpPercentage = 80;
        $scope.pop = false;

        $interval(function() {

            $scope.xpPercentage += 1;

        }, 20, 25).then(function() {

            $scope.xp += 1;
            $scope.pop = true;
            $scope.level += 1;
            return $timeout(function() {}, 500);

        }).then(function() {

            $scope.xp = 0;
            $scope.xpPercentage = 0;
            $scope.xpTarget = calculator.getXpForLevel($scope.newLevel+1) - calculator.getXpForLevel($scope.newLevel);
            $scope.pop = false;

        });


    }]);
