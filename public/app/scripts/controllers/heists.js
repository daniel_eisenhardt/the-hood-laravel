'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:HeistsCtrl
 * @description
 * # HeistsCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('HeistsCtrl',
    ['$scope', '$uibModal', '$q', 'heistsService', 'playerService', 'usersService', 'calculatorService', 'facebookService',
    function ($scope, $modal, $q, heistsService, player, usersService, calculator, facebook) {

        $scope.heists = [];
        $scope.planningHeist = false;
        $scope.player = player;
        $scope.tab = 'plan';
        $scope.loading = false;
        $scope.heistPlan = {
            heist: null,
            participants: {}
        };
        $scope.heistPlans = [];

        (function() { //constructor logic
            var heists = heistsService.query(function() {
                $scope.heists = heists;
                $scope.heistPlan.heist = heists[0];
            });
        }());

        $scope.$watch('heistPlan.heist', function(newValue) {
            if(newValue === null) {
                return;
            }

            $scope.heistPlan.participants = {};
            $scope.heistPlan.version = 0;

            $scope.heistPlan.participants[ newValue.heist_roles[0].id ] = player.getUser();
        });

        $scope.$watchGroup(['player.getHeistPlan()', 'heists'], function() {
            if(player.getHeistPlan() === null || typeof player.getHeistPlan() === 'undefined') {
                $scope.planningHeist = false;
            }
            else {
                $scope.planningHeist = true;
                $scope.loading = true;
                var promises = [];

                for(var i in $scope.heists) {
                    if($scope.heists[i].id == player.getHeistPlan().heist_id) {
                        $scope.heistPlan.heist = $scope.heists[i];
                    }
                }

                $scope.heistPlan.participants = {};
                angular.forEach(player.getHeistPlan().heist_participations, function(participation) {
                    $scope.heistPlan.participants[ participation.heist_role_id ] = {};
                    promises.push(
                        usersService.get({id: participation.user_id}, function(user) {
                            angular.extend($scope.heistPlan.participants[ participation.heist_role_id ], user);
                        }).$promise
                    );
                });

                $q.all(promises).then(function() {
                   $scope.loading = false;
                });
            }
        });

        $scope.$watchGroup(['player.getHeistPlans()', 'heists'], function() {

            var heistPlans = player.getHeistPlans();

            $scope.heistPlans = heistPlans;
        });

        $scope.getLevel = function (xp) {
            return calculator.getLevel(xp);
        };

        $scope.teamIsComplete = function()
        {
            return $scope.heistPlan.heist !== null && Object.keys($scope.heistPlan.participants).length == $scope.heistPlan.heist.heist_roles.length;
        };

        $scope.getPlannedHeist = function()
        {
            for(var i in $scope.heists) {
                if($scope.heists[i].id = player.getHeistPlan().id) {
                    return $scope.heists[i];
                }
            }

            return {};
        }

        $scope.showParticipantModal = function(role) {
            var scope = $scope.$new();
            scope.options = [player.getUser()];
            scope.loading = true;
            scope.role = role;

            usersService.friends(function(friends) {
                scope.options = scope.options.concat(friends);
                scope.loading = false;
            });

            var modalPromise = $modal.open({
                templateUrl: '/app/views/modals/choose_participant.html',
                scope: scope
            }).result;

            modalPromise.then(function (user) {
                angular.forEach($scope.heistPlan.participants, function(participant, key) {
                    if(participant.id === user.id) {
                        delete $scope.heistPlan.participants[key];
                    }
                });

                $scope.heistPlan.participants[ role.id ] = user;
                $scope.heistPlan.version ++; //To update success chance
            });
        };

        $scope.sendInvitations = function() {
            var roles = {};
            var facebookIds = [];

            angular.forEach($scope.heistPlan.participants, function(user, roleId) {
                roles[roleId] = user.id;
                if(user.id !== player.getId()) {
                    facebookIds.push(user.facebook_id);
                }
            });

            facebook.sendRequests(facebookIds, 'I want you for my ' + $scope.heistPlan.heist.name).then(function(requestOid) {

                heistsService.plan($scope.heistPlan.heist.id, roles, requestOid).then(function(response) {
                    player.setData(response.data.user);
                });

            });
        };

        $scope.cancelPlannedHeist = function() {
            heistsService.cancel(player.getHeistPlan().id).then(function(response) {
                player.setData(response.data.user);
            });
        };

    }]);
