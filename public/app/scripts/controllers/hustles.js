'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:HustlesCtrl
 * @description
 * # HustlesCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
    .controller('HustlesCtrl', ['$scope', '$interval', '$q', 'hustlesService', 'playerService', 'calculatorService', function ($scope, $interval, $q, hustlesService, player, calculator) {

        $scope.hustles = [];
        $scope.selected = {hustle: null};
        $scope.progress = 0;
        $scope.lastResult;
        $scope.lastResult = hustlesService.lastResult;
        $scope.player = player;

        var hustles = hustlesService.query(function () {
            $scope.hustles = hustles;
            $scope.selected.hustle = hustles[0];
            for(var i in hustles) {
                if(hustles[i].id === player.getLastHustleId()) {
                    $scope.selected.hustle = hustles[i];
                }
            }
        });

        $scope.getChance = function (hustle) {
            var ability = ( player.getLevel() + player.getAttributeLevel(hustle['attribute']) ) / 2;

            return Math.floor(calculator.getSuccessChance(ability, hustle['difficulty']) * 100);
        };

        $scope.execute = function () {
            var apiPromise = hustlesService.execute($scope.selected.hustle['id']);

            var timedPromise = $interval(function () {
                $scope.progress += 1;
            }, 10, 100);

            $q.all([apiPromise, timedPromise]).then(function (responses) {
                var response = responses[0];

                $scope.progress = 0;
                player.setData(response.data.user);
            });
        };

    }]);
