'use strict';

describe('Controller: LevelupCtrl', function () {

  // load the controller's module
  beforeEach(module('publicApp'));

  var LevelupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LevelupCtrl = $controller('LevelupCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LevelupCtrl.awesomeThings.length).toBe(3);
  });
});
