'use strict';

describe('Controller: HeistsCtrl', function () {

  // load the controller's module
  beforeEach(module('publicApp'));

  var HeistsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HeistsCtrl = $controller('HeistsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HeistsCtrl.awesomeThings.length).toBe(3);
  });
});
