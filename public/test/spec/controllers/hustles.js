'use strict';

describe('Controller: HustlesCtrl', function () {

  // load the controller's module
  beforeEach(module('publicApp'));

  var HustlesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HustlesCtrl = $controller('HustlesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HustlesCtrl.awesomeThings.length).toBe(3);
  });
});
