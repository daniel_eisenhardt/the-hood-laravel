'use strict';

describe('Controller: HeistplanCtrl', function () {

  // load the controller's module
  beforeEach(module('publicApp'));

  var HeistplanCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HeistplanCtrl = $controller('HeistplanCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HeistplanCtrl.awesomeThings.length).toBe(3);
  });
});
