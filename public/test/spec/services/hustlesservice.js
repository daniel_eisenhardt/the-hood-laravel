'use strict';

describe('Service: hustlesService', function () {

  // load the service's module
  beforeEach(module('publicApp'));

  // instantiate service
  var hustlesService;
  beforeEach(inject(function (_hustlesService_) {
    hustlesService = _hustlesService_;
  }));

  it('should do something', function () {
    expect(!!hustlesService).toBe(true);
  });

});
