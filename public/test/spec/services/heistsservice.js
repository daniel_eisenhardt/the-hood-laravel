'use strict';

describe('Service: heistsService', function () {

  // load the service's module
  beforeEach(module('publicApp'));

  // instantiate service
  var heistsService;
  beforeEach(inject(function (_heistsService_) {
    heistsService = _heistsService_;
  }));

  it('should do something', function () {
    expect(!!heistsService).toBe(true);
  });

});
