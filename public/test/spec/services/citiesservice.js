'use strict';

describe('Service: citiesService', function () {

  // load the service's module
  beforeEach(module('publicApp'));

  // instantiate service
  var citiesService;
  beforeEach(inject(function (_citiesService_) {
    citiesService = _citiesService_;
  }));

  it('should do something', function () {
    expect(!!citiesService).toBe(true);
  });

});
