'use strict';

describe('Service: responseDataInterceptor', function () {

  // instantiate service
  var responseDataInterceptor,
    init = function () {
      inject(function (_responseDataInterceptor_) {
        responseDataInterceptor = _responseDataInterceptor_;
      });
    };

  // load the service's module
  beforeEach(module('publicApp'));

  it('should do something', function () {
    init();

    expect(!!responseDataInterceptor).toBe(true);
  });

  it('should be configurable', function () {
    module(function (responseDataInterceptorProvider) {
      responseDataInterceptorProvider.setSalutation('Lorem ipsum');
    });

    init();

    expect(responseDataInterceptor.greet()).toEqual('Lorem ipsum');
  });

});
