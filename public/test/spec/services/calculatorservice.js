'use strict';

describe('Service: calculatorService', function () {

  // load the service's module
  beforeEach(module('publicApp'));

  // instantiate service
  var calculatorService;
  beforeEach(inject(function (_calculatorService_) {
    calculatorService = _calculatorService_;
  }));

  it('should do something', function () {
    expect(!!calculatorService).toBe(true);
  });

});
