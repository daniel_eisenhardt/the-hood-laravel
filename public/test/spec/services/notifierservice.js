'use strict';

describe('Service: notifierService', function () {

  // load the service's module
  beforeEach(module('publicApp'));

  // instantiate service
  var notifierService;
  beforeEach(inject(function (_notifierService_) {
    notifierService = _notifierService_;
  }));

  it('should do something', function () {
    expect(!!notifierService).toBe(true);
  });

});
